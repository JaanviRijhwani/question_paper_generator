# Question Paper Generator


A client based standalone application built on java.

## How it works?
 Generating Question Paper Algorithm works this way:
If Question Paper Difficulty Level:
1. EASY-
        50% with BEST probability Questions - easy/medium/hard
        25% with AVG probability Questions - easy/medium
        25% with NEW probability Questions - easy/medium

2. MEDIUM-
        50% with BEST probability Questions - easy/medium/hard
        25% with AVG probability Questions - easy/medium
        25% with NEW probability Questions - medium/hard

3. HARD-
        50% with BEST probability Questions - easy/medium/hard
        25% with AVG probability Questions - medium/hard
        25% with NEW probability Questions - medium/hard

## Functionalities
This application provides features such as:

1. CRUD Operations - 
It performs CRUD Operations on Semester, Branch, Subject, Chapter, Question. Also there is datatable associated with every Entity.

2. Download Sample Question Papers - 
It has option to download Sample Question Papers for Normal and MCQ type each of 80(sem) and 20(unit test) marks.

3. Download Previously Generated Question Papers - 
It has option to download previously generated question papers with specified date.

4. Generate Question Paper - 
It is the Major part of the application, where our algorithm works as explained above.

## ScreenShots

* Login Screen

![Login Screen](screenshots/login.png)

*   Functionalities Frame

![Login Screen](screenshots/functionalties_frame.png)

*   Add Branch 

![Login Screen](screenshots/branch_add.png)

*   Branch Index

![Login Screen](screenshots/branch_index.png)

*   Edit Branch

![Login Screen](screenshots/branch_edit.png)

*   Download Sample Paper Screen

![Login Screen](screenshots/sample_download.png)

*   Download Previous Question Paper Screen

![Login Screen](screenshots/previous_papers_download.png)

*   Generate Question Paper Screen

![Login Screen](screenshots/generate_question_paper.png)

DB Design

https://dbdiagram.io/d/61566ca4825b5b01461ba45c

