/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author user
 */
public class OptionPOJO {
    public int id;
    public String title;
    public QuestionPOJO question;
    public int isCorrect;
    
    public OptionPOJO(int id, String title, int isCorrect) {
        this.id = id;
        this.title = title;
        this.isCorrect = isCorrect;
    }
    public OptionPOJO(String title, int isCorrect) 
    {
        this.title = title;
        this.isCorrect = isCorrect;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    public String getTitle()
    {
        return this.title;
    }
    
    public QuestionPOJO getQuestion()
    {
        return this.question;
    }
    
    public int getIsCorrect()
    {
        return this.isCorrect;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    public void setQuestion(QuestionPOJO question)
    {
        this.question = question;
    }
    
    public void setIsCorrect(int isCorrect)
    {
        this.isCorrect = isCorrect;
    }
}
