/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

public class PreviousQuestionPaperPOJO {
    public int id;
    public BranchPOJO branch;
    public SemesterPOJO semester;
    public SubjectPOJO subject;
    public String type;
    public String marks;
    public String content;
    public String created_at;
    public String updated_at;
    
    public PreviousQuestionPaperPOJO(int id, BranchPOJO branch, SemesterPOJO semester, SubjectPOJO subject, String content , String type, String marks, String created_at, String updated_at) {
        this.id = id;
        this.branch = branch;
        this.semester = semester;
        this.subject = subject;
        this.content = content;
        this.type = type;
        this.marks = marks;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
    
    public int getId() {
        return id;
    }
    
    public BranchPOJO getBranch() {
        return branch;
    }
    
    public SemesterPOJO getSemester() {
        return semester;
    }
    
    public SubjectPOJO getSubject() {
        return subject;
    }
    
    public String getContent() {
        return content;
    }
    
    public String getCreatedAt() {
        return created_at;
    }
    
    public String getUpdatedAt() {
        return updated_at;
    }
    
    public String getType() {
        return type;
    }
    
    public String getMarks() {
        return marks;
    }
}