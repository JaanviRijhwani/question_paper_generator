/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author user
 */
public class SemesterPOJO {
    public int id;
    public String name;
    
    public SemesterPOJO(String name) {
        this.name = name;
    }
    
    public SemesterPOJO(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
}
