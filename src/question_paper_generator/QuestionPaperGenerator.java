/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator;

import POJO.QuestionPOJO;
import java.util.ArrayList;
import java.util.HashMap;
import question_paper_generator.Helper.DependencyInjector;
import question_paper_generator.Database.Database;
import question_paper_generator.Helper.DatabaseHelper;
import question_paper_generator.Helper.JSONHelper;
import question_paper_generator.Helper.PDFHelper;
import question_paper_generator.Helper.QuestionDetails;
import question_paper_generator.UI.Branches.BranchIndex;
import question_paper_generator.UI.Chapters.ChapterIndex;
import question_paper_generator.UI.ExamPaper.PreviousYearPapersIndex;
import question_paper_generator.UI.ExamPaper.SamplePaperIndex;
import question_paper_generator.UI.Generator.GeneratorIndex;
import question_paper_generator.UI.Index;
import question_paper_generator.UI.IndexFrame;
import question_paper_generator.UI.Login;
import question_paper_generator.UI.MainFrame;
import question_paper_generator.UI.Questions.QuestionIndex;
import question_paper_generator.UI.Semesters.SemesterIndex;
import question_paper_generator.UI.Subjects.SubjectIndex;

public class QuestionPaperGenerator {

    public static DependencyInjector di;
    public static void main(String[] args) {
        di = new DependencyInjector();
        Database db = new Database();
        DatabaseHelper databaseHelper = new DatabaseHelper();
        di.set("database", db);
        di.set("databaseHelper", databaseHelper);
        BranchIndex bi = new BranchIndex();
        di.set("branchesIndex", bi);
        SemesterIndex si = new SemesterIndex();
        di.set("semestersIndex", si);
        SubjectIndex subjectIndex = new SubjectIndex();
        di.set("subjectsIndex", subjectIndex);
        ChapterIndex chapterIndex = new ChapterIndex();
        di.set("chaptersIndex", chapterIndex);
        QuestionIndex questionIndex = new QuestionIndex();
        di.set("questionsIndex", questionIndex);
        Index index = new Index();
        di.set("index", index);
        GeneratorIndex generatorIndex = new GeneratorIndex();
        di.set("generatorIndex", generatorIndex);
        MainFrame mainFrame = new MainFrame();
        di.set("mainFrame", mainFrame);
        SamplePaperIndex samplePaperIndex = new SamplePaperIndex();
        di.set("samplePaperIndex", samplePaperIndex);
        PreviousYearPapersIndex previousYearPapersIndex = new PreviousYearPapersIndex();
        di.set("previousYearPapersIndex", previousYearPapersIndex);
        new Login(di).setVisible(true);
    }
    
    public static void generatePaper(HashMap<String, Object> details) {
        ArrayList<QuestionDetails> chapterDetails = PDFHelper.generateQuestionPaper(details);
        HashMap<String, ArrayList<QuestionPOJO>> questionSets = PDFHelper.allocateQuestions(chapterDetails, Integer.parseInt((String)details.get("marks")), (String)details.get("type"));
        String json = JSONHelper.convertQuestionsToJSON(questionSets);
        
        JSONHelper.setJSONInDatabase("question_paper", "content", json, details);
        DatabaseHelper dh = (DatabaseHelper) di.get("databaseHelper");
        dh.updateCount((String) details.get("type"));
        String type = (String)details.get("type");
        if(type.equalsIgnoreCase("NORMAL")) {
            dh.updateProbabilityAndCount(questionSets.get("5markQuestions"), questionSets.get("10markQuestions"), type);
        } else if(type.equalsIgnoreCase("MCQ")) {
            dh.updateProbabilityAndCount(questionSets.get("1markQuestions"), questionSets.get("2markQuestions"), type);  
        }
    }
}
