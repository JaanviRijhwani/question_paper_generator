/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator;

public interface Constants {
       
    public String NORMAL = "NORMAL";
    public String MCQ = "MCQ";
    
    public int UT_MARKS = 20;
    public int SEM_MARKS = 80;
    public int[] MARKS = new int[] { 20, 80 };
       
    public String[][] EASY_PAPER = {
        { 
            "easy",
            "medium",
            "hard" 
        },
        { 
            "easy",
            "medium", 
        },
        {
            "easy",
            "medium"
        }
    };
    public String[][] MEDIUM_PAPER = {
        { 
            "easy",
            "medium",
            "hard" 
        },
        { 
            "easy",
            "medium", 
        },
        {
            "medium",
            "hard"
        }
    };
    public String[][] HARD_PAPER = {
        { 
            "easy",
            "medium",
            "hard" 
        },
        { 
            "medium",
            "hard", 
        },
        {
            "medium",
            "hard"
        }
    };
    public int[] EASY_SEGREGATE = new int[] { 50, 25, 25};
    public int[] MEDIUM_SEGREGATE = new int[] { 35, 35, 30};
    public int[] HARD_SEGREGATE = new int[] { 25, 45, 30};
    
    public int FIVE_MARKS_QUESTIONS_IN_80_MARKS = 12;
    public int TEN_MARKS_QUESTIONS_IN_80_MARKS = 6;
    
    public int FIVE_MARKS_QUESTIONS_IN_20_MARKS = 3;
    public int TEN_MARKS_QUESTIONS_IN_20_MARKS = 2;
    
    public int ONE_MARKS_QUESTIONS_IN_80_MARKS_MCQ = 48;
    public int TWO_MARKS_QUESTIONS_IN_80_MARKS_MCQ = 16;
    
    public int ONE_MARKS_QUESTIONS_IN_20_MARKS_MCQ = 12;
    public int TWO_MARKS_QUESTIONS_IN_20_MARKS_MCQ = 4;
    
    
    public int MAXIMUM_REMAINING_QUESTION_PERCENTAGE = 75;
    public int MINIMUM_REMAINING_QUESTION_PERCENTAGE = 25;
    
    public int MCQ_TWO_MARKS_PERCENTAGE = 40;
    public int MCQ_ONE_MARKS_PERCENTAGE = 60;
}
