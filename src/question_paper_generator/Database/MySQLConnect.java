/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class MySQLConnect {
    
    private static final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/question_paper_generator";
    private static final String USERNAME ="root";
    private static final String PASSWORD = "";
    
    
    
    public static Connection getConnection(){
        Connection conn = null;
        
        try{
            conn = DriverManager.getConnection(CONNECTION_STRING,USERNAME,PASSWORD);
        }
        catch(SQLException ex){            
            JOptionPane.showMessageDialog(null,"Conncetion Failed :"+ ex.getMessage());
        }
        return conn;
        
    }
    public static void main(String args[]){
        Connection conn = getConnection();
    }

    
}
