/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.UI.Subjects;

import question_paper_generator.UI.Semesters.*;
import question_paper_generator.UI.Chapters.*;
import question_paper_generator.UI.Branches.*;
import POJO.BranchPOJO;
import POJO.SemesterPOJO;
import POJO.SubjectPOJO;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import javax.swing.JPanel;
import question_paper_generator.Helper.DatabaseHelper;
import question_paper_generator.Helper.DependencyInjector;
import question_paper_generator.Helper.UserInterfaceHelper;
import question_paper_generator.QuestionPaperGenerator;
import question_paper_generator.UI.Index;
import question_paper_generator.UI.IndexFrame;

public class SubjectOperations extends javax.swing.JPanel {

    /**
     * Creates new form CreateBranch
     */
    private DependencyInjector di;
    private String operation;
    private SubjectPOJO editableSubject;
    public SubjectOperations() {
        initComponents();
        setCreateOperation();
        di = QuestionPaperGenerator.di;
        this.setVisible(true);
        UserInterfaceHelper.addLabelMouseListener(btnSubmit);
        loadData();
    }
    
    public SubjectOperations(SubjectPOJO editableSubject) {
        initComponents();
        this.editableSubject = editableSubject;
        di = QuestionPaperGenerator.di;
        setEditOperation();
        updateOperationLabel();
        this.setVisible(true);
        UserInterfaceHelper.addLabelMouseListener(btnSubmit);
        loadData();
        setData();
    }
    
    private void setData() {
        this.txtName.setText(editableSubject.getName());
        this.branchComboBox.setSelectedItem(editableSubject.getBranch().getName());
        this.semesterComboBox.setSelectedItem(editableSubject.getSemester().getName());
    }
    
    private void loadData() {
        DatabaseHelper dh = (DatabaseHelper) di.get("databaseHelper");
        ArrayList<BranchPOJO> branches = dh.getBranches();
        ArrayList<SemesterPOJO> semesters = dh.getSemesters();
        
        for(BranchPOJO branch: branches) {
            branchComboBox.addItem(branch.getName());
        }
        for(SemesterPOJO semester: semesters) {
            semesterComboBox.addItem(semester.getName());
        }
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        btnBack = new javax.swing.JLabel();
        operationLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        lblName = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnSubmit = new javax.swing.JLabel();
        branchComboBox = new javax.swing.JComboBox<>();
        jSeparator5 = new javax.swing.JSeparator();
        lblName1 = new javax.swing.JLabel();
        semesterComboBox = new javax.swing.JComboBox<>();
        lblName3 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setBackground(new java.awt.Color(255, 255, 255));

        btnBack.setBackground(new java.awt.Color(89, 112, 129));
        btnBack.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        btnBack.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/question_paper_generator/UI/icons/back-button.png"))); // NOI18N
        btnBack.setToolTipText("");
        btnBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBackMouseClicked(evt);
            }
        });

        operationLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        operationLabel.setForeground(new java.awt.Color(89, 112, 129));
        operationLabel.setText("Create Subject");

        jLabel2.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));

        txtName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        txtName.setBorder(null);
        txtName.setDoubleBuffered(true);
        txtName.setMargin(new java.awt.Insets(0, 0, 0, 0));

        lblName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName.setText("Subject");

        jSeparator1.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        btnSubmit.setBackground(new java.awt.Color(89, 112, 129));
        btnSubmit.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        btnSubmit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSubmit.setText("Submit");
        btnSubmit.setToolTipText("");
        btnSubmit.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(89, 112, 129)));
        btnSubmit.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSubmitMouseClicked(evt);
            }
        });

        branchComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        branchComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Branch" }));
        branchComboBox.setBorder(null);
        branchComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        branchComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                branchComboBoxItemStateChanged(evt);
            }
        });

        jSeparator5.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        lblName1.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName1.setText("Branch");

        semesterComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        semesterComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Semester" }));
        semesterComboBox.setBorder(null);
        semesterComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        semesterComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                semesterComboBoxItemStateChanged(evt);
            }
        });

        lblName3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName3.setText("Semester");

        jSeparator4.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 547, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(63, 63, 63)
                                .addComponent(operationLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblName)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(86, 86, 86)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblName1)
                                .addGap(23, 23, 23)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(branchComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(226, 226, 226)
                        .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(51, 51, 51))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblName3)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(semesterComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(operationLabel)
                    .addComponent(btnBack))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblName)
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, 0)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(branchComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblName1))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblName3)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(semesterComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
                .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseClicked
        int res = 0;
        DatabaseHelper dh = (DatabaseHelper) di.get("databaseHelper");
        if(! "".equals(txtName.getText()) && branchComboBox.getSelectedIndex() != 0 && semesterComboBox.getSelectedIndex() != 0) {
            if("Create".equalsIgnoreCase(this.operation))
            {
                BranchPOJO branchPojo = dh.getBranchFromName(branchComboBox.getSelectedItem().toString());
                SemesterPOJO semesterPojo = dh.getSemesterFromName(semesterComboBox.getSelectedItem().toString());
                SubjectPOJO subject = new SubjectPOJO(txtName.getText(), branchPojo, semesterPojo);
                res = dh.insertSubject(subject);
            }                
            else if("Edit".equalsIgnoreCase(this.operation)) 
            {
                BranchPOJO branchPojo = dh.getBranchFromName(branchComboBox.getSelectedItem().toString());
                SemesterPOJO semesterPojo = dh.getSemesterFromName(semesterComboBox.getSelectedItem().toString());
                SubjectPOJO subject = new SubjectPOJO(editableSubject.getId(), txtName.getText(), branchPojo, semesterPojo);
                res = dh.updateSubject(subject);
            }
            UserInterfaceHelper.goBackToPanel(di, "subjectsIndex");
        }
    }//GEN-LAST:event_btnSubmitMouseClicked

    private void btnBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBackMouseClicked
        UserInterfaceHelper.goBackToPanel(di, "subjectsIndex");
    }//GEN-LAST:event_btnBackMouseClicked
    
    private void branchComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_branchComboBoxItemStateChanged
        
    }//GEN-LAST:event_branchComboBoxItemStateChanged

    private void semesterComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_semesterComboBoxItemStateChanged
        
    }//GEN-LAST:event_semesterComboBoxItemStateChanged

    private void setCreateOperation()
    {
        this.operation = "Create";
    }
    
    private void setEditOperation()
    {
        this.operation = "Edit";
    }
    
    private void updateOperationLabel()
    {
        operationLabel.setText(this.operation + " Subject");
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> branchComboBox;
    private javax.swing.JLabel btnBack;
    private javax.swing.JLabel btnSubmit;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblName1;
    private javax.swing.JLabel lblName3;
    private javax.swing.JLabel operationLabel;
    private javax.swing.JComboBox<String> semesterComboBox;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables

}
