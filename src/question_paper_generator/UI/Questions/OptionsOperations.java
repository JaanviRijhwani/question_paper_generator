/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.UI.Questions;

import question_paper_generator.UI.Chapters.*;
import question_paper_generator.UI.Branches.*;
import POJO.BranchPOJO;
import POJO.ChapterPOJO;
import POJO.OptionPOJO;
import POJO.QuestionPOJO;
import POJO.SemesterPOJO;
import POJO.SubjectPOJO;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import javax.swing.JPanel;
import question_paper_generator.Helper.DatabaseHelper;
import question_paper_generator.Helper.DependencyInjector;
import question_paper_generator.Helper.UserInterfaceHelper;
import question_paper_generator.QuestionPaperGenerator;
import question_paper_generator.UI.Index;
import question_paper_generator.UI.IndexFrame;

public class OptionsOperations extends javax.swing.JPanel {

    /**
     * Creates new form CreateBranch
     */
    private DependencyInjector di;
    private static int optionCount = 3;
    private QuestionPOJO question;
    private String operation;
    private ArrayList<OptionPanel> optionPanels = new ArrayList<>();
    private ArrayList<Integer> deletedOptionIds = new ArrayList<>();
    
    public OptionsOperations(QuestionPOJO question, boolean isEdit) {
        initComponents();
        this.question = question;
        optionPanel.setLayout(new GridLayout(5, 0));
        di = QuestionPaperGenerator.di;
        this.setVisible(true);
        UserInterfaceHelper.addLabelMouseListener(btnSubmit);
        this.validate();
        this.revalidate();
        this.repaint();
        setOperationOnOptionsPanel(isEdit);
        loadData();
    }
    
    private void setOperationOnOptionsPanel(boolean isEdit)
    {
        if(isEdit) {
            setEditOperation();
            updateOperationLabel();
            setData();
        } else {
            OptionPanel option1 = new OptionPanel(1, this);
            OptionPanel option2 = new OptionPanel(2, this);
            optionPanels.add(option1);
            optionPanels.add(option2);
            optionPanel.add(option1);
            optionPanel.add(option2);
            setCreateOperation();
        }
    }
    
    private void setData() {
        ArrayList<OptionPOJO> options = question.getOptions();
        OptionPanel panel = null;
        int count = 1;
        for(OptionPOJO option: options) {
            panel = new OptionPanel(count++, option, this);
            optionPanels.add(panel);
            optionPanel.add(panel);
        }
        this.optionCount = count;
    }
    
    private void loadData() {
        DatabaseHelper dh = (DatabaseHelper) di.get("databaseHelper");
        titleTextArea.setEditable(false);
        titleTextArea.setText(this.question.getTitle());
    }
    
    public void removePanel(OptionPanel option) {
        if(! (optionPanels.size() <= 2)) {
            if(option.getId() != 0) {
                deletedOptionIds.add(option.getId());
            }
            optionPanels.remove(option);
            optionPanel.remove(option);
            optionPanel.validate();
            optionPanel.revalidate();
            optionPanel.repaint();
            this.validate();
            this.revalidate();
            this.repaint();
        }
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        jRadioButton1 = new javax.swing.JRadioButton();
        btnBack = new javax.swing.JLabel();
        operationLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnSubmit = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        titleTextArea = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        optionsPanel = new javax.swing.JPanel();
        jSeparator2 = new javax.swing.JSeparator();
        addOptionBtn = new javax.swing.JLabel();
        lblName2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        optionPanel = new javax.swing.JPanel();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jRadioButton1.setText("jRadioButton1");

        setBackground(new java.awt.Color(255, 255, 255));

        btnBack.setBackground(new java.awt.Color(89, 112, 129));
        btnBack.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        btnBack.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/question_paper_generator/UI/icons/back-button.png"))); // NOI18N
        btnBack.setToolTipText("");
        btnBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBackMouseClicked(evt);
            }
        });

        operationLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        operationLabel.setForeground(new java.awt.Color(89, 112, 129));
        operationLabel.setText("Create Options For Question");

        jLabel2.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));

        lblName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName.setText("Question");

        jSeparator1.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        btnSubmit.setBackground(new java.awt.Color(89, 112, 129));
        btnSubmit.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        btnSubmit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSubmit.setText("Submit");
        btnSubmit.setToolTipText("");
        btnSubmit.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(89, 112, 129)));
        btnSubmit.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSubmitMouseClicked(evt);
            }
        });

        titleTextArea.setColumns(20);
        titleTextArea.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        titleTextArea.setRows(5);
        titleTextArea.setBorder(null);
        titleTextArea.setOpaque(false);
        jScrollPane1.setViewportView(titleTextArea);

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane2.setBorder(null);
        jScrollPane2.setForeground(new java.awt.Color(255, 255, 255));

        optionsPanel.setBackground(new java.awt.Color(255, 255, 255));

        jSeparator2.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        addOptionBtn.setBackground(new java.awt.Color(89, 112, 129));
        addOptionBtn.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        addOptionBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addOptionBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/question_paper_generator/UI/icons/plus.png"))); // NOI18N
        addOptionBtn.setToolTipText("");
        addOptionBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        addOptionBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addOptionBtnMouseClicked(evt);
            }
        });

        lblName2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName2.setText("Options");

        jScrollPane3.setBorder(null);

        optionPanel.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout optionPanelLayout = new javax.swing.GroupLayout(optionPanel);
        optionPanel.setLayout(optionPanelLayout);
        optionPanelLayout.setHorizontalGroup(
            optionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 643, Short.MAX_VALUE)
        );
        optionPanelLayout.setVerticalGroup(
            optionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 204, Short.MAX_VALUE)
        );

        jScrollPane3.setViewportView(optionPanel);

        javax.swing.GroupLayout optionsPanelLayout = new javax.swing.GroupLayout(optionsPanel);
        optionsPanel.setLayout(optionsPanelLayout);
        optionsPanelLayout.setHorizontalGroup(
            optionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(optionsPanelLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(lblName2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(addOptionBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
            .addComponent(jSeparator2)
            .addGroup(optionsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 660, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        optionsPanelLayout.setVerticalGroup(
            optionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(optionsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(optionsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(addOptionBtn)
                    .addComponent(lblName2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jScrollPane2.setViewportView(optionsPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(63, 63, 63)
                        .addComponent(operationLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(lblName)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jSeparator1)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 564, Short.MAX_VALUE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())))
            .addGroup(layout.createSequentialGroup()
                .addGap(264, 264, 264)
                .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(operationLabel)
                    .addComponent(btnBack))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblName)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private ArrayList<OptionPOJO> getValidatedOptions(boolean isEdit)
    {
        int optionsFilledCount = 0;
        boolean areAtLeasetOneOptionCorrect = false;
        ArrayList<OptionPOJO> options = new ArrayList<>();
        for(OptionPanel optionPanel: this.optionPanels) {
            if(! "".equals(optionPanel.getOptionText())) {
                optionsFilledCount++;
                if(optionPanel.getIsCorrect() == 1) {
                    areAtLeasetOneOptionCorrect = true;
                }
                OptionPOJO option = null;
                if(! isEdit) {
                    option = new OptionPOJO(optionPanel.getOptionText(),
                        optionPanel.getIsCorrect());
                } else {
                    option = new OptionPOJO(optionPanel.getId(), optionPanel.getOptionText(),
                        optionPanel.getIsCorrect());
                }
                options.add(option);
            }
        }
        if(optionsFilledCount >= 2 && areAtLeasetOneOptionCorrect)
        {
            return options;
        }
        return null;
    }
    private void btnSubmitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseClicked
      
        DatabaseHelper dh = (DatabaseHelper)di.get("databaseHelper");
        if("Create".equalsIgnoreCase(this.operation))
        {
            ArrayList<OptionPOJO> options = this.getValidatedOptions(false);
            if(options != null) {
                question.setOptions(options);
                dh.insertQuestion(question);
            }
        } else if("Edit".equalsIgnoreCase(this.operation)) 
        {
            ArrayList<OptionPOJO> options = this.getValidatedOptions(true);
            if(options != null) {
                question.setOptions(options);
                dh.updateQuestion(question, deletedOptionIds);
            }
        }  
        UserInterfaceHelper.goBackToPanel(di, "questionsIndex");
    }//GEN-LAST:event_btnSubmitMouseClicked

    private void btnBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBackMouseClicked
        // TODO: show previous Question Add Screen!
    }//GEN-LAST:event_btnBackMouseClicked

    private void addOptionBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addOptionBtnMouseClicked
        if(optionPanels.size() < 5) {
            OptionPanel option = new OptionPanel(optionCount++, this);
            optionPanels.add(option);
            optionPanel.add(option);
            this.validate();
            this.revalidate();
            this.repaint();
        }
    }//GEN-LAST:event_addOptionBtnMouseClicked

    private void setCreateOperation()
    {
        this.operation = "Create";
    }
    
    private void setEditOperation()
    {
        this.operation = "Edit";
    }
    
    private void updateOperationLabel()
    {
        operationLabel.setText(this.operation + " Options For Question");
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel addOptionBtn;
    private javax.swing.JLabel btnBack;
    private javax.swing.JLabel btnSubmit;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblName2;
    private javax.swing.JLabel operationLabel;
    private javax.swing.JPanel optionPanel;
    private javax.swing.JPanel optionsPanel;
    private javax.swing.JTextArea titleTextArea;
    // End of variables declaration//GEN-END:variables

}
