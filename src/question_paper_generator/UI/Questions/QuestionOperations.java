/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.UI.Questions;

import question_paper_generator.UI.Chapters.*;
import question_paper_generator.UI.Branches.*;
import POJO.BranchPOJO;
import POJO.ChapterPOJO;
import POJO.QuestionPOJO;
import POJO.SemesterPOJO;
import POJO.SubjectPOJO;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import javax.swing.JPanel;
import question_paper_generator.Helper.DatabaseHelper;
import question_paper_generator.Helper.DependencyInjector;
import question_paper_generator.Helper.UserInterfaceHelper;
import question_paper_generator.QuestionPaperGenerator;
import question_paper_generator.UI.Index;
import question_paper_generator.UI.IndexFrame;

public class QuestionOperations extends javax.swing.JPanel {

    /**
     * Creates new form CreateBranch
     */
    private DependencyInjector di;
    private String operation;
    private QuestionPOJO editableQuestion;
    public QuestionOperations() {
        initComponents();
        di = QuestionPaperGenerator.di;
        UserInterfaceHelper.addLabelMouseListener(btnSubmit);
        this.setVisible(true);
        setCreateOperation();
        loadData();
    }
    
    public QuestionOperations(QuestionPOJO editableQuestion)
    {
        initComponents();
        this.editableQuestion = editableQuestion;
        di = QuestionPaperGenerator.di;
        UserInterfaceHelper.addLabelMouseListener(btnSubmit);
        setEditOperation();
        updateOperationLabel();
        loadData();
        setData();
        this.setVisible(true);
    }
    
    private void setData() {    
        this.titleTextArea.setText(editableQuestion.getTitle());
        this.branchComboBox.setSelectedItem(editableQuestion.getChapter().getSubject().getBranch().getName());
        this.semesterComboBox.setSelectedItem(editableQuestion.getChapter().getSubject().getSemester().getName());
        this.subjectComboBox.setSelectedItem(editableQuestion.getChapter().getSubject().getName());
        this.chapterComboBox.setSelectedItem(editableQuestion.getChapter().getName());
        this.difficultyLevelComboBox.setSelectedItem(editableQuestion.getDifficultyLevel());
        this.typeComboBox.setSelectedItem(editableQuestion.getType());
        this.marksComboBox.setSelectedItem(String.valueOf(editableQuestion.getMarks()));
    }
    
    private void loadData() {
        DatabaseHelper dh = (DatabaseHelper) di.get("databaseHelper");
        ArrayList<BranchPOJO> branches = dh.getBranches();
        ArrayList<SemesterPOJO> semesters = dh.getSemesters();
        ArrayList<String> questionTypes = dh.getQuestionTypes();
        ArrayList<String> questionDifficultyLevels = dh.getQuestionDifficultyLevels();
        
        for(BranchPOJO branch: branches) {
            branchComboBox.addItem(branch.getName());
        }
        
        for(SemesterPOJO semester: semesters) {
            semesterComboBox.addItem(semester.getName());
        }
        
        for(String questionType: questionTypes) {
            typeComboBox.addItem(questionType);
        }
        
        for(String questionDifficultyLevel: questionDifficultyLevels) {
            difficultyLevelComboBox.addItem(questionDifficultyLevel);
        }
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        btnBack = new javax.swing.JLabel();
        operationLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnSubmit = new javax.swing.JLabel();
        lblName1 = new javax.swing.JLabel();
        lblName2 = new javax.swing.JLabel();
        semesterComboBox = new javax.swing.JComboBox<>();
        lblName3 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        branchComboBox = new javax.swing.JComboBox<>();
        jSeparator5 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        titleTextArea = new javax.swing.JTextArea();
        lblName4 = new javax.swing.JLabel();
        chapterComboBox = new javax.swing.JComboBox<>();
        jSeparator6 = new javax.swing.JSeparator();
        subjectComboBox = new javax.swing.JComboBox<>();
        jSeparator7 = new javax.swing.JSeparator();
        typeComboBox = new javax.swing.JComboBox<>();
        lblName5 = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        difficultyLevelComboBox = new javax.swing.JComboBox<>();
        jSeparator9 = new javax.swing.JSeparator();
        lblName6 = new javax.swing.JLabel();
        marksComboBox = new javax.swing.JComboBox<>();
        lblName7 = new javax.swing.JLabel();
        jSeparator10 = new javax.swing.JSeparator();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setBackground(new java.awt.Color(255, 255, 255));

        btnBack.setBackground(new java.awt.Color(89, 112, 129));
        btnBack.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        btnBack.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/question_paper_generator/UI/icons/back-button.png"))); // NOI18N
        btnBack.setToolTipText("");
        btnBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBackMouseClicked(evt);
            }
        });

        operationLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        operationLabel.setForeground(new java.awt.Color(89, 112, 129));
        operationLabel.setText("Create Question");

        jLabel2.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));

        lblName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName.setText("Question");

        jSeparator1.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        btnSubmit.setBackground(new java.awt.Color(89, 112, 129));
        btnSubmit.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        btnSubmit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnSubmit.setText("Submit");
        btnSubmit.setToolTipText("");
        btnSubmit.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(89, 112, 129)));
        btnSubmit.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSubmitMouseClicked(evt);
            }
        });

        lblName1.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName1.setText("Branch");

        lblName2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName2.setText("Subject");

        semesterComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        semesterComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Semester" }));
        semesterComboBox.setBorder(null);
        semesterComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        semesterComboBox.setPreferredSize(new java.awt.Dimension(105, 24));
        semesterComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                semesterComboBoxItemStateChanged(evt);
            }
        });
        semesterComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                semesterComboBoxActionPerformed(evt);
            }
        });

        lblName3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName3.setText("Semester");

        jSeparator4.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        branchComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        branchComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Branch" }));
        branchComboBox.setBorder(null);
        branchComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        branchComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                branchComboBoxItemStateChanged(evt);
            }
        });

        jSeparator5.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        titleTextArea.setColumns(20);
        titleTextArea.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        titleTextArea.setRows(5);
        titleTextArea.setBorder(null);
        titleTextArea.setOpaque(false);
        jScrollPane1.setViewportView(titleTextArea);

        lblName4.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName4.setText("Chapter");

        chapterComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        chapterComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Chapter" }));
        chapterComboBox.setBorder(null);
        chapterComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        chapterComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chapterComboBoxItemStateChanged(evt);
            }
        });

        jSeparator6.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator6.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        subjectComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        subjectComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Subject" }));
        subjectComboBox.setBorder(null);
        subjectComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        subjectComboBox.setPreferredSize(new java.awt.Dimension(105, 24));
        subjectComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                subjectComboBoxItemStateChanged(evt);
            }
        });

        jSeparator7.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        typeComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        typeComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Question Type" }));
        typeComboBox.setBorder(null);
        typeComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        typeComboBox.setPreferredSize(new java.awt.Dimension(105, 24));
        typeComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                typeComboBoxItemStateChanged(evt);
            }
        });

        lblName5.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName5.setText("Type");

        jSeparator8.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        difficultyLevelComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        difficultyLevelComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Difficulty Level" }));
        difficultyLevelComboBox.setBorder(null);
        difficultyLevelComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        difficultyLevelComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                difficultyLevelComboBoxItemStateChanged(evt);
            }
        });

        jSeparator9.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator9.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        lblName6.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName6.setText("Level");

        marksComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        marksComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Marks" }));
        marksComboBox.setBorder(null);
        marksComboBox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        marksComboBox.setPreferredSize(new java.awt.Dimension(105, 24));
        marksComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                marksComboBoxItemStateChanged(evt);
            }
        });

        lblName7.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        lblName7.setText("Marks");

        jSeparator10.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(89, 112, 129)));
        jSeparator10.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblName)
                                .addGap(37, 37, 37)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jSeparator1)
                                    .addComponent(jScrollPane1)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblName3)
                                            .addComponent(lblName2))
                                        .addGap(23, 23, 23)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(jSeparator7)
                                                .addComponent(subjectComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(jSeparator4)
                                                .addComponent(semesterComboBox, 0, 161, Short.MAX_VALUE))
                                            .addComponent(typeComboBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jSeparator8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(63, 63, 63)
                                        .addComponent(operationLabel))
                                    .addComponent(lblName5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 72, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblName4)
                                        .addGap(15, 15, 15)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jSeparator6)
                                            .addComponent(chapterComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblName1)
                                        .addGap(23, 23, 23)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jSeparator5)
                                            .addComponent(branchComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(lblName6)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jSeparator9)
                                            .addComponent(difficultyLevelComboBox, 0, 214, Short.MAX_VALUE))))))
                        .addGap(37, 37, 37))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(86, 86, 86)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(marksComboBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jSeparator10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(lblName7))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(277, 277, 277))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(operationLabel)
                    .addComponent(btnBack))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(39, 39, 39)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblName)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, 0)
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(semesterComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblName3))
                                .addGap(0, 0, 0)
                                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(40, 40, 40)
                                        .addComponent(lblName2))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(30, 30, 30)
                                        .addComponent(subjectComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblName1)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(branchComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(35, 35, 35)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(chapterComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblName4))
                                .addGap(0, 0, 0)
                                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblName5)
                            .addComponent(typeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, 0)
                        .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(difficultyLevelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblName6))
                        .addGap(0, 0, 0)
                        .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName7)
                    .addComponent(marksComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseClicked
        DatabaseHelper dh = (DatabaseHelper)di.get("databaseHelper");
        if(! ("".equals(titleTextArea.getText()) && 
                semesterComboBox.getSelectedIndex() == 0 && 
                branchComboBox.getSelectedIndex() == 0 &&
                chapterComboBox.getSelectedIndex() == 0 &&
                subjectComboBox.getSelectedIndex() == 0 &&
                typeComboBox.getSelectedIndex() == 0 &&
                difficultyLevelComboBox.getSelectedIndex() == 0)) {
            if("Create".equalsIgnoreCase(this.operation))
            {
                 QuestionPOJO question = new QuestionPOJO(
                    titleTextArea.getText(),
                    difficultyLevelComboBox.getSelectedItem().toString(),
                    Integer.parseInt(marksComboBox.getSelectedItem().toString()),
                    dh.getChapterFromName(chapterComboBox.getSelectedItem().toString()),
                    typeComboBox.getSelectedItem().toString()
                );
                 
                if("MCQ".equals(typeComboBox.getSelectedItem())) {
                   if(di.has("lastIndex")) {
                        ((IndexFrame)di.get("lastIndex")).dispose();
                        IndexFrame frame = new IndexFrame(700, 600);
                        frame.setVisible(true);
                        frame.add(new OptionsOperations(question, false));
                        frame.repaint();
                        frame.revalidate();
                        frame.validate();
                        di.set("lastIndex", frame);
                    }     
                } else {
                    int questionId = dh.insertQuestion(question);
                }
            }
            else if("Edit".equalsIgnoreCase(this.operation)) 
            {
                QuestionPOJO question = new QuestionPOJO(
                    editableQuestion.getId(),
                    titleTextArea.getText(),
                    difficultyLevelComboBox.getSelectedItem().toString(),
                    Integer.parseInt(marksComboBox.getSelectedItem().toString()),
                    typeComboBox.getSelectedItem().toString(),
                    editableQuestion.getProbability(),
                    dh.getChapterFromName(chapterComboBox.getSelectedItem().toString()),
                    editableQuestion.getOptions()
                );
                if("MCQ".equals(typeComboBox.getSelectedItem())) {
                   if(di.has("lastIndex")) {
                        ((IndexFrame)di.get("lastIndex")).dispose();
                        IndexFrame frame = new IndexFrame(700, 600);
                        frame.setVisible(true);
                        frame.add(new OptionsOperations(question, true));
                        frame.repaint();
                        frame.revalidate();
                        frame.validate();
                        di.set("lastIndex", frame);
                    }     
                } else {
                    int questionId = dh.insertQuestion(question);
                    UserInterfaceHelper.goBackToPanel(di, "questionsIndex");
                }
            }
        }
        
    }//GEN-LAST:event_btnSubmitMouseClicked

    private void btnBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBackMouseClicked
        if(di.has("indexFrame")) {
            ((IndexFrame)di.get("indexFrame")).dispose();
        }
        if(di.has("index")) {
            Index index = (Index)di.get("index");
            index.setVisible(true);
            index.replacePanel(index.getRelatedPanel(),(JPanel)di.get("questionsIndex"));
        }
    }//GEN-LAST:event_btnBackMouseClicked

    private void branchComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_branchComboBoxItemStateChanged
        setSubjectComboBox(evt);
    }//GEN-LAST:event_branchComboBoxItemStateChanged

    private void semesterComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_semesterComboBoxItemStateChanged
        setSubjectComboBox(evt);
    }//GEN-LAST:event_semesterComboBoxItemStateChanged

    private void chapterComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chapterComboBoxItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_chapterComboBoxItemStateChanged

    private void subjectComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_subjectComboBoxItemStateChanged
        if(evt.getStateChange() == ItemEvent.SELECTED) {
            if(subjectComboBox.getSelectedIndex() != 0 )
            fetchChapterDetails(subjectComboBox.getSelectedItem().toString());
        }
    }//GEN-LAST:event_subjectComboBoxItemStateChanged

    private void typeComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_typeComboBoxItemStateChanged
        if(evt.getStateChange() == ItemEvent.DESELECTED) {
            marksComboBox.removeAllItems();
            marksComboBox.addItem("Select Marks");
        }else if(evt.getStateChange() == ItemEvent.SELECTED) {
            DatabaseHelper dh = (DatabaseHelper)di.get("databaseHelper");
            ArrayList<String> marks = dh.getQuestionMarks();
            for(String mark: marks) {
                if("NORMAL".equals(typeComboBox.getSelectedItem())) {
                    if(! mark.equals("1") && ! mark.equals("2")) {
                        marksComboBox.addItem(mark);
                    }
                } else if("MCQ".equals(typeComboBox.getSelectedItem())) {
                    if( mark.equals("1") || mark.equals("2")) {
                        marksComboBox.addItem(mark);
                    }
                }
            }
        }
    }//GEN-LAST:event_typeComboBoxItemStateChanged

    private void difficultyLevelComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_difficultyLevelComboBoxItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_difficultyLevelComboBoxItemStateChanged

    private void semesterComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_semesterComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_semesterComboBoxActionPerformed

    private void marksComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_marksComboBoxItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_marksComboBoxItemStateChanged

    private void setSubjectComboBox(ItemEvent evt)
    {
        if(evt.getStateChange() == ItemEvent.DESELECTED) {
            subjectComboBox.removeAllItems();
            subjectComboBox.addItem("Select Subject");
        }
        if(evt.getStateChange() == ItemEvent.SELECTED) {
            if(branchComboBox.getSelectedIndex() != 0 
                && semesterComboBox.getSelectedIndex() != 0)
            fetchSubjectDetails(branchComboBox.getSelectedItem().toString(), semesterComboBox.getSelectedItem().toString());
        }
    }
    private void fetchSubjectDetails(String branchName, String semesterName)
    {
        DatabaseHelper dh = (DatabaseHelper) di.get("databaseHelper");
        BranchPOJO branch = new BranchPOJO(branchName);
        SemesterPOJO semester = new SemesterPOJO(semesterName);
        
        ArrayList<SubjectPOJO> subjects = 
                dh.getSubjectFromBranchAndSemester(branch, semester);
        for(SubjectPOJO subject: subjects) {
            subjectComboBox.addItem(subject.getName());
        }
    }
    
    private void fetchChapterDetails(String subject)
    {
        DatabaseHelper dh = (DatabaseHelper) di.get("databaseHelper");
        SubjectPOJO subjectPOJO = new SubjectPOJO(subject);
        ArrayList<ChapterPOJO> chapters = dh.getChapterFromSubject(subjectPOJO);
        for(ChapterPOJO chapter: chapters) {
            chapterComboBox.addItem(chapter.getName());
        }
    }

    private void setCreateOperation()
    {
        this.operation = "Create";
    }
    
    private void setEditOperation()
    {
        this.operation = "Edit";
    }
    
    private void updateOperationLabel()
    {
        operationLabel.setText(this.operation + " Question");
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> branchComboBox;
    private javax.swing.JLabel btnBack;
    private javax.swing.JLabel btnSubmit;
    private javax.swing.JComboBox<String> chapterComboBox;
    private javax.swing.JComboBox<String> difficultyLevelComboBox;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblName1;
    private javax.swing.JLabel lblName2;
    private javax.swing.JLabel lblName3;
    private javax.swing.JLabel lblName4;
    private javax.swing.JLabel lblName5;
    private javax.swing.JLabel lblName6;
    private javax.swing.JLabel lblName7;
    private javax.swing.JComboBox<String> marksComboBox;
    private javax.swing.JLabel operationLabel;
    private javax.swing.JComboBox<String> semesterComboBox;
    private javax.swing.JComboBox<String> subjectComboBox;
    private javax.swing.JTextArea titleTextArea;
    private javax.swing.JComboBox<String> typeComboBox;
    // End of variables declaration//GEN-END:variables

}
