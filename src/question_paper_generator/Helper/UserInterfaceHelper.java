/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.Helper;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import question_paper_generator.UI.Index;
import question_paper_generator.UI.IndexFrame;


public class UserInterfaceHelper {
    public static void goBackToPanel(DependencyInjector di, String frame)
    {
        if(di.has("lastIndex")) {
            ((IndexFrame)di.get("lastIndex")).dispose();
            di.unset("lastIndex");
        }
        if(di.has("index")) {
            Index index = (Index)di.get("index");
            index.setVisible(true);
            index.replacePanel(index.getRelatedPanel(),(JPanel)di.get(frame));
        }
    }
    
    public static void addButtonMouseListener(JPanel panel, JLabel label) {
        panel.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseEntered(MouseEvent e) {
            panel.setBackground(new Color(89, 112, 129));
            label.setForeground(new Color(255, 255, 255));
            panel.setCursor(new Cursor(Cursor.HAND_CURSOR));
            panel.validate();
         }
         
         @Override          
         public void mouseExited(MouseEvent e) {
            panel.setBackground(new Color(255, 255, 255));
            label.setForeground(new Color(0, 0, 0));
            panel.setCursor(null);
            panel.validate();
         }
      });
    }

    public static void addLabelMouseListener(JLabel label) {
        label.addMouseListener(new MouseAdapter() {
         @Override
         public void mouseEntered(MouseEvent e) {
            label.setOpaque(true);
            label.setBackground(new Color(89, 112, 129));
            label.setForeground(new Color(255, 255, 255));
            label.validate();
         }
         
         @Override          
         public void mouseExited(MouseEvent e) {
            label.setOpaque(true);
            label.setBackground(new Color(255, 255, 255));
            label.setForeground(new Color(0, 0, 0));
            label.validate();
         }
      });
    }
}