/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.Helper;

import POJO.ChapterPOJO;
import POJO.QuestionPOJO;
import java.util.HashMap;
import java.util.Random;


public class QuestionDetails implements Comparable<QuestionDetails>{
    ChapterPOJO chapter;
    int pendingMarks;
    // Will store 5 marks questions if NORMAL and 1 marks questions if MCQ
    HashMap<Integer, QuestionPOJO> questionSet1;
    
    // Will store 10 marks questions if NORMAL and 2 marks questions if MCQ
    HashMap<Integer, QuestionPOJO> questionSet2;
    
    public QuestionDetails(ChapterPOJO chapter) {
        this.questionSet1 = new HashMap<>();
        this.questionSet2 = new HashMap<>();
        this.chapter = chapter;
    }
    public int getChapterId() {
        return this.chapter.getId();
    }
    public ChapterPOJO getChapter() {
        return this.chapter;
    }
    public int getMarks() {
        return this.chapter.getWeight();
    }
    
    public void setPendingMarks(int pendingMarks) {
        this.pendingMarks = pendingMarks;
    }
    public int getPendingMarks() {
        return this.pendingMarks;
    }
    private int updatePendingMarks(int marks) {
        this.pendingMarks -= marks;
        return this.pendingMarks;
    }
    
    public boolean pushQuestionInSet1(QuestionPOJO questionPOJO) {
        this.updatePendingMarks(questionPOJO.getMarks());
        if(this.questionSet1.containsKey(questionPOJO.getId())) {
            return false;
        }
        this.questionSet1.put(questionPOJO.getId(), questionPOJO);
        return true;
    }
    public boolean removeQuestionFromSet1(QuestionPOJO questionPOJO) {
        this.updatePendingMarks(-(questionPOJO.getMarks()));
        if(this.questionSet1.containsKey(questionPOJO.getId())) {
            this.questionSet1.remove(questionPOJO);
            return true;
        }
        return false;
    }
    public boolean checkIfQuestionExistsInSet1(QuestionPOJO questionPOJO) {
        return this.questionSet1.containsKey(questionPOJO.getId());
    }
    public boolean checkIfQuestionExistsInSet1(int id) {
        return this.questionSet1.containsKey(id);
    }
    
    public boolean pushQuestionInSet2(QuestionPOJO questionPOJO) {
        this.updatePendingMarks(questionPOJO.getMarks());
        if(this.questionSet2.containsKey(questionPOJO.getId())) {
            return false;
        }
        this.questionSet2.put(questionPOJO.getId(), questionPOJO);
        return true;
    }
    public boolean removeQuestionFromSet2(QuestionPOJO questionPOJO) {
        this.updatePendingMarks(-(questionPOJO.getMarks()));
        if(this.questionSet2.containsKey(questionPOJO.getId())) {
            this.questionSet2.remove(questionPOJO);
            return true;
        }
        return false;
    }
    public boolean checkIfQuestionExistsInSet2(QuestionPOJO questionPOJO) {
        return this.questionSet2.containsKey(questionPOJO.getId());
    }
    public boolean checkIfQuestionExistsInSet2(int id) {
        return this.questionSet2.containsKey(id);
    }
    
    public QuestionPOJO randomQuestionFromQuestionSet1() {
        if(!questionSet1.isEmpty()) {
            Random generator = new Random();
            Object[] values = questionSet1.values().toArray();
            return (QuestionPOJO) values[generator.nextInt(values.length)];
        }
        return null;
    }
    
    public QuestionPOJO randomQuestionFromQuestionSet2() {
        if(!questionSet2.isEmpty()) {
            Random generator = new Random();
            Object[] values = questionSet2.values().toArray();
            return (QuestionPOJO) values[generator.nextInt(values.length)];
        }
        return null;
    }
    public int sizeOfSet2Questions() {
        return questionSet2.size();
    }
    public int sizeOfSet1Questions() {
        return questionSet1.size();
    }

    @Override
    public int compareTo(QuestionDetails questionDetails) {
        if(this.getMarks() < questionDetails.getMarks()) {
            return 1;
        } else if (this.getMarks() > questionDetails.getMarks()) {
            return -1;
        }
        return 0;
    }
}
