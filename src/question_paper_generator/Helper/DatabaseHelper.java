/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.Helper;

import POJO.BranchPOJO;
import POJO.ChapterPOJO;
import POJO.OptionPOJO;
import POJO.QuestionPOJO;
import POJO.SemesterPOJO;
import POJO.SubjectPOJO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import question_paper_generator.Database.Database;
import POJO.PreviousQuestionPaperPOJO;
import question_paper_generator.QuestionPaperGenerator;


public class DatabaseHelper {
    private PreparedStatement ps;
    private ResultSet rs;
    private Statement stmt;
    private DependencyInjector di;
    
    public DatabaseHelper() 
    {
        this.di = QuestionPaperGenerator.di;
    }
    
    public ArrayList<BranchPOJO> getBranches()
    {
        ArrayList<BranchPOJO> branches = new ArrayList<>();
        try {
            Database db = (Database)di.get("database");
            rs = db.table("branches").where("", "is_deleted = 0").get();
            while(rs.next()) {
                branches.add(new BranchPOJO(rs.getInt("id"), rs.getString("name")));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return branches;
    }
    
    public int insertBranch(BranchPOJO branchPOJO) 
    { 
        Database db = (Database)di.get("database");
        int rowCount = db.table("branches").where("and", "is_deleted = 0", "name = "+branchPOJO.getName()).count();
        if(rowCount > 0)
            return 0;
        HashMap<String, String> branch = new HashMap<>();
        branch.put("name", branchPOJO.getName());
        return db.table("branches").insert(branch);
    }
    
    public int updateBranch(BranchPOJO branchPOJO) 
    { 
        Database db = (Database)di.get("database");
        int rowCount = db.table("branches").where("and", 
                "is_deleted = 0", 
                "name = "+branchPOJO.getName(), 
                "id != " + branchPOJO.getId()
        ).count();
        if(rowCount > 0)
            return 0;
        HashMap<String, String> branch = new HashMap<>();
        branch.put("name", branchPOJO.getName());
        
        return db.table("branches").update(branch, "id = " + branchPOJO.getId());
    }
    
    public ArrayList<SemesterPOJO> getSemesters()
    {
        ArrayList<SemesterPOJO> semesters = new ArrayList<>();
        try {
            Database db = (Database)di.get("database");
            rs = db.table("semesters").where("", "is_deleted = 0").get();
            while(rs.next()) {
                semesters.add(new SemesterPOJO(rs.getInt("id"), rs.getString("name")));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return semesters;
    }
    
    public int insertSemester(SemesterPOJO semesterPOJO) 
    { 
        Database db = (Database)di.get("database");
        int rowCount = db.table("semesters").where("and", "is_deleted = 0", "name = "+semesterPOJO.getName()).count();
        if(rowCount > 0)
            return 0;
        HashMap<String, String> semester = new HashMap<>();
        semester.put("name", semesterPOJO.getName());
        return db.table("semesters").insert(semester);
    }
    
    public int updateSemester(SemesterPOJO semesterPOJO) 
    { 
        Database db = (Database)di.get("database");
        int rowCount = db.table("semesters").where("and", 
                "is_deleted = 0", 
                "name = "+semesterPOJO.getName(), 
                "id != " + semesterPOJO.getId()
        ).count();
        if(rowCount > 0)
            return 0;
        HashMap<String, String> semester = new HashMap<>();
        semester.put("name", semesterPOJO.getName());
        
        return db.table("semesters").update(semester, "id = " + semesterPOJO.getId());
    }
     
    public ArrayList<SubjectPOJO> getSubjects() 
    {
        ArrayList<SubjectPOJO> subjects = new ArrayList<>();
        try {
            Database db = (Database)di.get("database");
            rs = db.table("subjects, branches, semesters, branch_semester_subject")
                    .where("and", "subjects.is_deleted = 0", 
                            "branch_semester_subject.subject_id = ~subjects.id",
                            "branches.id = ~branch_semester_subject.branch_id",
                            "semesters.id = ~branch_semester_subject.semester_id")
                    .get();
            while(rs.next()) {
                subjects.add(new SubjectPOJO(rs.getInt("subjects.id"), 
                        rs.getString("subjects.name"), 
                        new BranchPOJO(rs.getString("branches.name")), 
                        new SemesterPOJO(rs.getString("semesters.name"))));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return subjects;
    }
    
    public int insertSubject(SubjectPOJO subjectPOJO) 
    { 
        try {
            Database db = (Database)di.get("database");
            ResultSet rs = db.table("subjects")
                    .where("and", "is_deleted = 0", "name = "+subjectPOJO.getName())
                    .get();
            
            if(rs.next()) {
                int subjectId = rs.getInt("id");
                HashMap<String, String> subjectDetails = new HashMap<>();
                subjectDetails.put("subject_id", String.valueOf(subjectId));
                subjectDetails.put("branch_id", String.valueOf(subjectPOJO.getBranch().getId()));
                subjectDetails.put("semester_id", String.valueOf(subjectPOJO.getSemester().getId()));
                db.table("branch_semester_subject").insert(subjectDetails);
                return subjectId;
            }
            HashMap<String, String> subject = new HashMap<>();
            subject.put("name", subjectPOJO.getName());
            int subjectId = db.table("subjects").insert(subject);
            HashMap<String, String> subjectDetails = new HashMap<>();
            subjectDetails.put("subject_id", String.valueOf(subjectId));
            subjectDetails.put("branch_id", String.valueOf(subjectPOJO.getBranch().getId()));
            subjectDetails.put("semester_id", String.valueOf(subjectPOJO.getSemester().getId()));
            db.table("branch_semester_subject").insert(subjectDetails);
            return subjectId;
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return 0;
    }
    
    
    public int updateSubject(SubjectPOJO subjectPOJO) 
    { 
        Database db = (Database)di.get("database");
        int rowCount = db.table("subjects").where("and", 
                "is_deleted = 0", 
                "name = "+subjectPOJO.getName(), 
                "id != " + subjectPOJO.getId()
        ).count();
        if(rowCount > 0)
            return 0;
        HashMap<String, String> subject = new HashMap<>();
        subject.put("name", subjectPOJO.getName());
        HashMap<String, String> subjectDetails = new HashMap<>();
        subjectDetails.put("subject_id", String.valueOf(subjectPOJO.getId()));
        subjectDetails.put("branch_id", String.valueOf(subjectPOJO.getBranch().getId()));
        subjectDetails.put("semester_id", String.valueOf(subjectPOJO.getSemester().getId()));
        db.table("branch_semester_subject").update(subjectDetails, 
                "branch_semester_subject.subject_id = " + subjectPOJO.getId());
        int subjectId = db.table("subjects").update(subject, "id = " + subjectPOJO.getId());
        return subjectId;
    }
           
    public ArrayList<ChapterPOJO> getChapters() 
    {
        ArrayList<ChapterPOJO> chapters = new ArrayList<>();
        try {
            Database db = (Database)di.get("database");
            rs = db.table("chapters, subjects")
                    .where("AND", "subjects.id = ~chapters.subject_id", "chapters.is_deleted = 0")
                    .get();
            while(rs.next()) {
                chapters.add(new ChapterPOJO(rs.getInt("id"), 
                        rs.getString("name"), 
                        rs.getInt("weight"),
                        new SubjectPOJO(
                                rs.getInt("subjects.id"), 
                                rs.getString("subjects.name")
                        )
                ));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return chapters;
    }
    
    public int insertChapter(ChapterPOJO chapterPOJO) 
    { 
        Database db = (Database)di.get("database");
        int rowCount = db.table("chapters")
                .where("and", "is_deleted = 0", 
                        "name = "+ chapterPOJO.getName(), 
                        "weight = "+chapterPOJO.getWeight(),
                        "subject_id = "+ chapterPOJO.getSubject().getId())
                .count();
        if(rowCount > 0)
            return 0;
        HashMap<String, String> chapter = new HashMap<>();
        chapter.put("name", chapterPOJO.getName());
        chapter.put("weight", String.valueOf(chapterPOJO.getWeight()));
        chapter.put("subject_id", String.valueOf(chapterPOJO.getSubject().getId()));
        return db.table("chapters").insert(chapter);
    }
    
    public int updateChapter(ChapterPOJO chapterPOJO) 
    { 
        Database db = (Database)di.get("database");
        int rowCount = db.table("chapters").where("and", 
                "is_deleted = 0", 
                "name = "+chapterPOJO.getName(), 
                "weight = "+chapterPOJO.getWeight(),
                "subject_id = "+ chapterPOJO.getSubject().getId(),
                "id != " + chapterPOJO.getId()
        ).count();
        if(rowCount > 0)
            return 0;
        HashMap<String, String> chapter = new HashMap<>();
        chapter.put("name", chapterPOJO.getName());
        chapter.put("weight", String.valueOf(chapterPOJO.getWeight()));
        chapter.put("subject_id", String.valueOf(chapterPOJO.getSubject().getId()));
        
        return db.table("chapters").update(chapter, "id = " + chapterPOJO.getId());
    }
    
    public ArrayList<QuestionPOJO> getQuestions(){
        ArrayList<QuestionPOJO> questions = new ArrayList<>();
        try {
            Database db = (Database)di.get("database");
            rs = db.table("questions, subjects, chapters").
                    where("and", "questions.is_deleted = 0", 
                            "subjects.id = ~chapters.subject_id", 
                            "chapters.id = ~questions.chapter_id"
                    ).get();
            while(rs.next()) {
                questions.add(new QuestionPOJO(
                        rs.getInt("id"), 
                        rs.getString("title"),
                        rs.getString("difficulty_level"),
                        rs.getInt("marks"),
                        rs.getString("type"),
                        rs.getInt("probability"),
                        new ChapterPOJO(rs.getInt("chapters.id"), 
                            rs.getString("chapters.name"), 
                            rs.getInt("weight"),
                            new SubjectPOJO(
                                    rs.getInt("subjects.id"), 
                                    rs.getString("subjects.name")
                            )
                        )
                ));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return questions;
    }
    
    public ArrayList<QuestionPOJO> getQuestions(String type){
        ArrayList<QuestionPOJO> questions = new ArrayList<>();
        try {
            Database db = (Database)di.get("database");
            rs = db.table("questions, subjects, chapters").
                    where("and", "questions.is_deleted = 0", 
                            "subjects.id = ~chapters.subject_id", 
                            "chapters.id = ~questions.chapter_id",
                            "questions.type = " + type
                    ).get();
            while(rs.next()) {
                questions.add(new QuestionPOJO(
                        rs.getInt("id"), 
                        rs.getString("title"),
                        rs.getString("difficulty_level"),
                        rs.getInt("marks"),
                        rs.getString("type"),
                        rs.getInt("probability"),
                        new ChapterPOJO(rs.getInt("chapters.id"), 
                            rs.getString("chapters.name"),
                            rs.getInt("chapters.weight"),
                            new SubjectPOJO(
                                    rs.getInt("subjects.id"), 
                                    rs.getString("subjects.name")
                            )
                        )
                ));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return questions;
    }
    
    public int insertQuestion(QuestionPOJO questionPOJO) 
    { 
        Database db = (Database)di.get("database");
        int rowCount = db.table("questions")
                .where("and", "is_deleted = 0", 
                        "title = "+ questionPOJO.getTitle(), 
                        "chapter_id = " + questionPOJO.getChapter().getId(),
                        "type = " + questionPOJO.getType(),
                        "marks = " + questionPOJO.getMarks(),
                        "difficulty_level = " + questionPOJO.getDifficultyLevel())
                .count();
        if(rowCount > 0)
            return 0;
        HashMap<String, String> question = new HashMap<>();
        question.put("title", questionPOJO.getTitle());
        question.put("difficulty_level", questionPOJO.getDifficultyLevel());
        question.put("marks", String.valueOf(questionPOJO.getMarks()));
        question.put("type", questionPOJO.getType());
        question.put("chapter_id", String.valueOf(questionPOJO.getChapter().getId()));
        int questionId = db.table("questions").insert(question);
        HashMap<String, String> questionOptions = new HashMap<>();
        if("MCQ".equals(questionPOJO.getType())) {
            for(OptionPOJO optionPojo: questionPOJO.getOptions()) {
                questionOptions.put("title", optionPojo.getTitle());
                questionOptions.put("question_id", String.valueOf(questionId));
                questionOptions.put("is_correct", String.valueOf(optionPojo.getIsCorrect()));
                int questionOption = db.table("question_options").insert(questionOptions);
            }
        }
        return questionId;
    }
    
    public int updateQuestion(QuestionPOJO questionPOJO, ArrayList<Integer> deletedOptionIds) 
    {
        Database db = (Database)di.get("database");
        int rowCount = db.table("questions")
                .where("and", "is_deleted = 0", 
                        "title = "+ questionPOJO.getTitle(), 
                        "chapter_id = " + questionPOJO.getChapter().getId(),
                        "type = " + questionPOJO.getType(),
                        "marks = " + questionPOJO.getMarks(),
                        "difficulty_level = " + questionPOJO.getDifficultyLevel(),
                        "id != " + questionPOJO.getId())
                .count();
        if(rowCount > 0)
            return 0;
        HashMap<String, String> question = new HashMap<>();
        question.put("title", questionPOJO.getTitle());
        question.put("difficulty_level", questionPOJO.getDifficultyLevel());
        question.put("marks", String.valueOf(questionPOJO.getMarks()));
        question.put("type", questionPOJO.getType());
        question.put("chapter_id", String.valueOf(questionPOJO.getChapter().getId()));
        db.table("questions").update(question, "id = " + questionPOJO.getId());
        HashMap<String, String> questionOptions = new HashMap<>();
        if("MCQ".equals(questionPOJO.getType())) {
            if(deletedOptionIds.size() > 0) {
                db.table("question_options").delete("id IN ( " + db.implodeFromList(deletedOptionIds) + " )");
            }
            for(OptionPOJO optionPojo: questionPOJO.getOptions()) {
                questionOptions.put("title", optionPojo.getTitle());
                questionOptions.put("question_id", String.valueOf(questionPOJO.getId()));
                questionOptions.put("is_correct", String.valueOf(optionPojo.getIsCorrect()));
                if(optionPojo.getId() == 0) {
                    int questionOption = db.table("question_options").insert(questionOptions);
                } else {
                    db.table("question_options").update(questionOptions, "id = " + optionPojo.getId());
                }
            }
        }
        return questionPOJO.getId();
    }
    
    public ArrayList<String> getQuestionTypes()
    {
        ArrayList<String> types = new ArrayList<>(); 
        try {
            Database db = (Database)di.get("database");
            rs = db.query("SHOW COLUMNS FROM questions LIKE 'type'");
            rs.next();
            String type = rs.getString("type");
            Pattern typePattern = Pattern.compile("'(.*?)'");
            Matcher typeMatcher = typePattern.matcher(type);
            while(typeMatcher.find()){
                types.add(typeMatcher.group(1));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return types;
    }
    
    public ArrayList<String> getQuestionMarks()
    {
        ArrayList<String> questionMarks = new ArrayList<>(); 
        try {
            Database db = (Database)di.get("database");
            rs = db.query("SHOW COLUMNS FROM questions LIKE 'marks'");
            rs.next();
            String type = rs.getString("type");
            Pattern typePattern = Pattern.compile("'(.*?)'");
            Matcher typeMatcher = typePattern.matcher(type);
            while(typeMatcher.find()){
                questionMarks.add(typeMatcher.group(1));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return questionMarks;
    }
    
    public ArrayList<String> getQuestionDifficultyLevels()
    {
        ArrayList<String> difficultyLevels = new ArrayList<>();
        try {
            Database db = (Database)di.get("database");
            rs = db.query("SHOW COLUMNS FROM questions LIKE 'difficulty_level'");
            rs.next();
            String type = rs.getString("type");
            Pattern typePattern = Pattern.compile("'(.*?)'");
            Matcher typeMatcher = typePattern.matcher(type);
            while(typeMatcher.find()){
                difficultyLevels.add(typeMatcher.group(1));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return difficultyLevels;
    }
    
    public QuestionPOJO getQuestionFromId(int id) {
        QuestionPOJO question = null;
        ArrayList<OptionPOJO> options = new ArrayList<>();
        try {
            Database db = (Database)di.get("database");
            rs = db.table("questions, chapters")
                    .where("AND", 
                            "questions.is_deleted = 0", 
                            "questions.id = " + id, 
                            "chapters.id = ~questions.chapter_id"
                    )
                    .get();
            while(rs.next()) {
                question = new QuestionPOJO(
                        rs.getInt("questions.id"),
                        rs.getString("questions.title"),
                        rs.getString("questions.difficulty_level"),
                        rs.getInt("questions.marks"),
                        rs.getString("questions.type"),
                        rs.getInt("questions.probability"),
                        this.getChapterFromName(rs.getString("chapters.name"))
                );
            }
            if("MCQ".equalsIgnoreCase(question.getType())) {
                rs = db.table("question_options")
                    .where("AND", 
                            "question_options.is_deleted = 0", 
                            "question_options.question_id = " + question.getId()
                    )
                    .get();
                while(rs.next()) {
                    options.add(new OptionPOJO(
                            rs.getInt("question_options.id"), 
                            rs.getString("question_options.title"),
                            rs.getInt("question_options.is_correct"))
                    );
                }
                question.setOptions(options);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return question;
    }
    
    public ChapterPOJO getChapterFromName(String name) {
        ChapterPOJO chapter = null;
        try {
            Database db = (Database)di.get("database");
            rs = db.table("chapters, subjects")
                    .where("AND", 
                            "chapters.is_deleted = 0", 
                            "chapters.name = " + name,
                            "subjects.id = ~chapters.subject_id"
                    )
                    .get();
            while(rs.next()) {
                chapter = new ChapterPOJO(
                        rs.getInt("chapters.id"), 
                        rs.getString("chapters.name"),
                        rs.getInt("weight"),
                        this.getSubjectFromName(rs.getString("subjects.name"))
                        
                );
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return chapter;
    }
    
    public ArrayList<SubjectPOJO> getSubjectFromBranchAndSemester
        (BranchPOJO branch, SemesterPOJO semester)
    {
        ArrayList<SubjectPOJO> subjects = new ArrayList<>();
        try {
            Database db = (Database)di.get("database");
            rs = db.table("subjects, branches, semesters, branch_semester_subject")
                    .where("and", "subjects.is_deleted = 0", 
                            "subjects.id = ~branch_semester_subject.subject_id",
                            "branch_semester_subject.branch_id = ~branches.id",
                            "branch_semester_subject.semester_id = ~semesters.id",
                            "semesters.name = " + semester.getName(),
                            "branches.name = " + branch.getName()
                    ).get();
            while(rs.next()) {
                subjects.add(new SubjectPOJO(rs.getInt("id"), rs.getString("name")));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return subjects;
    }
       
    public ArrayList<ChapterPOJO> getChapterFromSubject(SubjectPOJO subjectPOJO)
    {
        ArrayList<ChapterPOJO> chapters = new ArrayList<>();
        try {
            Database db = (Database)di.get("database");
            rs = db.table("chapters, subjects")
                    .where("and", "chapters.is_deleted = 0",
                            "chapters.subject_id = ~subjects.id",
                            "subjects.name = " + subjectPOJO.getName()
                    ).get();
            while(rs.next()) {
                chapters.add(new ChapterPOJO(rs.getInt("id"), 
                        rs.getString("name"), rs.getInt("weight"), subjectPOJO));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return chapters;
    }
    
    public SubjectPOJO getSubjectFromName(String name) 
    {
        SubjectPOJO subject = null;
        try {
            Database db = (Database)di.get("database");
            rs = db.table("subjects, branches, semesters, branch_semester_subject")
                    .where(
                            "and", 
                            "subjects.is_deleted = 0", 
                            "branch_semester_subject.subject_id = ~subjects.id",
                            "branches.id = ~branch_semester_subject.branch_id",
                            "semesters.id = ~branch_semester_subject.semester_id",
                            "subjects.name = " + name
                    ).get();
            while(rs.next()) {
                subject = new SubjectPOJO(
                        rs.getInt("subjects.id"), 
                        rs.getString("subjects.name"), 
                        new BranchPOJO(rs.getInt("branches.id"), rs.getString("branches.name")),
                        new SemesterPOJO(rs.getInt("semesters.id"), rs.getString("semesters.name"))
                );
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return subject;
    }
    
     public SubjectPOJO getSubjectFromId(int id) 
    {
        SubjectPOJO subject = null;
        try {
            Database db = (Database)di.get("database");
            rs = db.table("subjects, branches, semesters, branch_semester_subject")
                    .where(
                            "and", 
                            "subjects.is_deleted = 0", 
                            "branch_semester_subject.subject_id = ~subjects.id",
                            "branches.id = ~branch_semester_subject.branch_id",
                            "semesters.id = ~branch_semester_subject.semester_id",
                            "subjects.id = " + id
                    ).get();
            while(rs.next()) {
                subject = new SubjectPOJO(
                        rs.getInt("subjects.id"), 
                        rs.getString("subjects.name"), 
                        new BranchPOJO(rs.getInt("branches.id"), rs.getString("branches.name")),
                        new SemesterPOJO(rs.getInt("semesters.id"), rs.getString("semesters.name"))
                );
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return subject;
    }

    public BranchPOJO getBranchFromName(String name) 
    {
        BranchPOJO branch = null;
        try {
            Database db = (Database)di.get("database");
            rs = db.table("branches")
                    .where("and", "is_deleted = 0", "name = " + name)
                    .get();
            while(rs.next()) {
                branch = new BranchPOJO(rs.getInt("id"), rs.getString("name"));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return branch;
    }

    public BranchPOJO getBranchFromId(int id) 
    {
        BranchPOJO branch = null;
        try {
            Database db = (Database)di.get("database");
            rs = db.table("branches")
                    .where("and", "is_deleted = 0", "id = " + id)
                    .get();
            while(rs.next()) {
                branch = new BranchPOJO(rs.getInt("id"), rs.getString("name"));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return branch;
    }

    public SemesterPOJO getSemesterFromName(String name) 
    {
        SemesterPOJO semester = null;
        try {
            Database db = (Database)di.get("database");
            rs = db.table("semesters")
                    .where("and", 
                            "is_deleted = 0", 
                            "name = " + name
                    )
                    .get();
            while(rs.next()) {
                semester = new SemesterPOJO(rs.getInt("id"), rs.getString("name"));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return semester;
    }

    public SemesterPOJO getSemesterFromId(int id) 
    {
        SemesterPOJO semester = null;
        try {
            Database db = (Database)di.get("database");
            rs = db.table("semesters")
                    .where("and", 
                            "is_deleted = 0", 
                            "id = " + id
                    )
                    .get();
            while(rs.next()) {
                semester = new SemesterPOJO(rs.getInt("id"), rs.getString("name"));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return semester;
    }
    
    public HashMap<Integer, PreviousQuestionPaperPOJO> getPreviousYearsQuestionPapers() {
        HashMap<Integer, PreviousQuestionPaperPOJO> questionPapers = new HashMap<>();
        try {
            Database db = (Database)di.get("database");
            DatabaseHelper dh = (DatabaseHelper)di.get("databaseHelper");
            ResultSet tempRs  = db.table("question_paper")
                    .where("and", 
                            "is_deleted = 0"
                    )
                    .get();
            PreviousQuestionPaperPOJO questionPaper = null;
            while(tempRs.next()) {
                int id = tempRs.getInt("id");
                int branchId = tempRs.getInt("branch_id");
                int semesterId = tempRs.getInt("semester_id");
                int subjectId = tempRs.getInt("subject_id");
                String jsonContent = tempRs.getString("content");
                String type = tempRs.getString("type");
                String marks = tempRs.getString("marks");
                String createdAt = tempRs.getString("created_at");
                String updatedAt = tempRs.getString("created_at");
                questionPaper = new PreviousQuestionPaperPOJO(
                        id,
                        dh.getBranchFromId(branchId),
                        dh.getSemesterFromId(semesterId),
                        dh.getSubjectFromId(subjectId),
                        jsonContent,
                        type,
                        marks,
                        createdAt,
                        updatedAt
                );
                questionPapers.put(id, questionPaper);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return questionPapers;
    }
    
    public void updateCount(String questionType) {
        try {
            Database db = (Database)di.get("database");
            ResultSet rs = db.table("question_paper_count").where("and",
                    "type = " + questionType)
                    .get();
            if(rs.next()) {
                int prevCount = rs.getInt("count");
                HashMap<String, String> details = new HashMap<>();
                details.put("count", String.valueOf(prevCount+1));
                db.table("question_paper_count").update(details, "type = '" + questionType + "'");
            } else {
                HashMap<String, String> details = new HashMap<>();
                details.put("type", questionType);
                details.put("count", "1");
                db.table("question_paper_count").insert(details);
            }            
        } catch (SQLException ex) {}
    }
    
    public void updateProbabilityAndCount(ArrayList<QuestionPOJO> questionSet1, ArrayList<QuestionPOJO> questionSet2, String type) {
        questionSet1.addAll(questionSet2);
        String questionSetsIds = questionSet1.stream().map( e -> e.getId()).collect(Collectors.toList()).toString();
        questionSetsIds = questionSetsIds.replace('[', '(');
        questionSetsIds = questionSetsIds.replace(']', ')');
        
        Database db = (Database)di.get("database");
        // Updating Question count
        db.queryUpdate("UPDATE questions SET questions.count = questions.count + 1 WHERE questions.id IN " + questionSetsIds);
        
        // Updating 
        db.queryUpdate("UPDATE questions, question_paper_count SET questions.probability = TRUNCATE(CAST(questions.count as decimal) / CAST(question_paper_count.count as decimal), 2)*10 WHERE question_paper_count.type = '" + type + "' AND questions.id IN " + questionSetsIds);
        
        db.queryUpdate("UPDATE questions, question_paper_count SET questions.probability = TRUNCATE(CAST(questions.count as decimal) / CAST(question_paper_count.count as decimal), 2)*10 WHERE question_paper_count.type = '" + type + "' AND questions.id NOT IN " + questionSetsIds);
    }
}
