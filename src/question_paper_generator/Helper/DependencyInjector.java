/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.Helper;

import java.util.HashMap;

public class DependencyInjector {
    private final HashMap<String, Object> dependencies = new HashMap<>();
    public void set(String key, Object value){
        dependencies.put(key, value);
    }
    public void unset(String key){
        dependencies.remove(key);
    }
    public Object get(String key){
        if(dependencies.containsKey(key))
          return dependencies.get(key);
        return null;
    }
    public boolean has(String key) {
        return dependencies.containsKey(key);
    }
}
