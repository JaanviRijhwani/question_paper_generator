/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.Helper;

import javax.swing.table.DefaultTableModel;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

public class DataTableHelper 
{
    public static DefaultTableModel getMyTableModel(String [] colHeads, String editableCell)
    {
        DefaultTableModel model = new DefaultTableModel(colHeads,0){            
            @Override
            public boolean isCellEditable(int row,int col){
                return (editableCell.equals(colHeads[col]));
          }            
        };
        return model;
    }
}