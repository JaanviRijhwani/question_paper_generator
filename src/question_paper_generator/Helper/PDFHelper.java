/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question_paper_generator.Helper;

import POJO.OptionPOJO;
import POJO.QuestionPOJO;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.sun.corba.se.impl.orbutil.closure.Constant;
 import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import question_paper_generator.Constants;
import question_paper_generator.Database.Database;
import POJO.ChapterPOJO;
import POJO.OptionPOJO;
import POJO.QuestionPOJO;
import question_paper_generator.QuestionPaperGenerator;

public class PDFHelper {
    private static DependencyInjector di;
    
    static {
        PDFHelper.di = QuestionPaperGenerator.di;
    }

    private static String generateRandomWords(int numberOfWords)
    {
        StringBuffer randomString = new StringBuffer();
        Random random = new Random();
        for(int i = 0; i < numberOfWords; i++)
        {
            char[] word = new char[random.nextInt(8)+3]; // words of length 3 through 10. (1 and 2 letter words are boring.)
            for(int j = 0; j < word.length; j++)
            {
                word[j] = (char)('a' + random.nextInt(26));
            }
            randomString.append(" ");
            randomString.append(word);
        }
        return randomString.toString();
    }
    
    public static void generateSamplePDF(int marks, String type)
    {
        try {
            OutputStream file = null;
            String name = "question_paper_"+(new Date().getTime())+".pdf";
            file = new FileOutputStream(new File(name));
            Document document = new Document();
            PdfWriter.getInstance(document, file);
            document.open();
            Paragraph collegeName = new Paragraph("Thadomal Shahani Engineering College");
            Paragraph subjectName = new Paragraph("Subject : Java");
            Paragraph marksLabel = new Paragraph("Marks : " + marks);
            subjectName.setAlignment(Element.ALIGN_CENTER);
            collegeName.setAlignment(Element.ALIGN_CENTER);
            marksLabel.setAlignment(Element.ALIGN_RIGHT);
            document.add(collegeName);
            document.add(subjectName);
            document.add(marksLabel);
            document.addTitle("Subject: Java");
            LineSeparator ls = new LineSeparator();
            ls.setOffset(-10);
            document.add(ls);
            if(marks == 80  && "Normal".equalsIgnoreCase(type)){
                Paragraph attempt1 = new Paragraph("\n Attempt any 2 out of 4 \t\t (5 marks)");
                Paragraph attempt2 = new Paragraph("\n Attempt any 3 out of 4 \t\t (5 marks)");
                Paragraph attempt3 = new Paragraph("\n Attempt any 3 out of 4 \t\t (5 marks)");
                Paragraph attempt4 = new Paragraph("\n Attempt any 2 out of 3 \t\t (10 marks)");
                Paragraph attempt5 = new Paragraph("\n Attempt any 2 out of 3 \t\t (10 marks)");
                Paragraph questionsSet1 = new Paragraph("\n\t 1. What is Polymorphism?\n\t 2. Explain Garbage Collector in Detail.\n\t 3. Explain features of Java. \n\t 4. Explain static keyword");
                Paragraph questionsSet2 = new Paragraph("\n\t 1. What is Polymorphism?\n\t 2. Explain Garbage Collector in Detail.\n\t 3. Explain features of Java. \n\t 4. Explain static keyword");
                Paragraph questionsSet3 = new Paragraph("\n\t 1. What is Polymorphism?\n\t 2. Explain Garbage Collector in Detail.\n\t 3. Explain features of Java. \n\t 4. Explain static keyword");
                Paragraph questionsSet4 = new Paragraph("\n\t 1. What is Polymorphism?\n\t 2. Explain Garbage Collector in Detail.\n\t 3. Explain features of Java. ");
                Paragraph questionsSet5 = new Paragraph("\n\t 1. WAP to perform calculator operations\n\t 2. Packages in java \n\t 3. Explain Thread life-cycle in detail");
                document.add(attempt1);
                document.add(questionsSet1);
                document.add(attempt2);
                document.add(questionsSet2);
                document.add(attempt3);
                document.add(questionsSet3);
                document.add(attempt4);
                document.add(questionsSet4);
                document.add(attempt5);
                document.add(questionsSet5);
                document.addCreationDate();
                document.close();
            } else if(marks == 20 && "Normal".equalsIgnoreCase(type)) {
                Paragraph attempt1 = new Paragraph("\n Attempt any 2 out of 3 \t\t (5 marks)");
                Paragraph attempt2 = new Paragraph("\n Attempt any 1 out of 2 \t\t (10 marks)");
                Paragraph questionsSet1 = new Paragraph("\n\t 1. What is Polymorphism?\n\t 2. Explain Garbage Collector in Detail.\n\t 3. Explain features of Java.");
                Paragraph questionsSet2 = new Paragraph("\n\t 1. WAP to perform calculator operations \n\t 3. Explain Thread life-cycle in detail");
                document.add(attempt1);
                document.add(questionsSet1);
                document.add(attempt2);
                document.add(questionsSet2);
                document.addCreationDate();
                document.close();
            } else if (marks == 80 && "MCQ".equalsIgnoreCase(type)) {
                Paragraph attempt1 = new Paragraph("\nQ1. Solve Multiple Choice Questions:- \t\t (1 marks)");
                Paragraph attempt2 = new Paragraph("\nQ2. Solve Multiple Choice Questions:- \t\t (2 marks)");
                document.add(attempt1);
                for(int i=0; i<Constants.ONE_MARKS_QUESTIONS_IN_80_MARKS_MCQ; ++i) {
                    Paragraph question = new Paragraph("\n\t" + (i+1) + ". " + generateRandomWords(5));
                    document.add(question);
                    char ch = 'a';
                    for(int j=0; j<3; ++j) {
                        Paragraph option = new Paragraph(((ch == 'a') ? "\n\t" : "") + (ch++) + ") " + generateRandomWords(5));
                        document.add(option);
                    }
                }
                document.add(attempt2);
                for(int i=0; i<Constants.TWO_MARKS_QUESTIONS_IN_80_MARKS_MCQ; ++i) {
                    Paragraph question = new Paragraph("\n\t" + (i+1) + ". " + generateRandomWords(5));
                    document.add(question);
                    char ch = 'a';
                    for(int j=0; j<3; ++j) {
                        Paragraph option = new Paragraph(((ch == 'a') ? "\n\t" : "") + (ch++) + ") " + generateRandomWords(5));
                        document.add(option);
                    }
                }
                document.addCreationDate();
                document.close();
            } else if (marks == 20 && "MCQ".equalsIgnoreCase(type)) {
                Paragraph attempt1 = new Paragraph("\nQ1. Solve Multiple Choice Questions:- \t\t (1 marks)");
                Paragraph attempt2 = new Paragraph("\nQ2. Solve Multiple Choice Questions:- \t\t (2 marks)");
                document.add(attempt1);
                for(int i=0; i<Constants.ONE_MARKS_QUESTIONS_IN_20_MARKS_MCQ; ++i) {
                    Paragraph question = new Paragraph("\n\t" + (i+1) + ". " + generateRandomWords(5));
                    document.add(question);
                    char ch = 'a';
                    for(int j=0; j<3; ++j) {
                        Paragraph option = new Paragraph(((ch == 'a') ? "\n\t" : "") + (ch++) + ") " + generateRandomWords(5));
                        document.add(option);
                    }
                }
                document.add(attempt2);
                for(int i=0; i<Constants.TWO_MARKS_QUESTIONS_IN_20_MARKS_MCQ; ++i) {
                    Paragraph question = new Paragraph("\n\t" + i+1 + ". " + generateRandomWords(5));
                    document.add(question);
                    char ch = 'a';
                    for(int j=0; j<3; ++j) {
                        Paragraph option = new Paragraph(((ch == 'a') ? "\n\t" : "") + (ch++) + ") " + generateRandomWords(5));
                        document.add(option);
                    }
                }
                document.addCreationDate();
                document.close();
            }
            JOptionPane.showMessageDialog(null, "Sample Question Paper has been Generated Successfully!");
        } catch (FileNotFoundException | DocumentException ex) {
            JOptionPane.showMessageDialog(null, "There was some problem while generating Sample Question Paper!");
        }
    }
    
    public static ArrayList<QuestionDetails> generateQuestionPaper(HashMap<String, Object> details) {
        // Fill question details per chapter.
        ArrayList<QuestionDetails> chapterDetails = new ArrayList<>();
        
        // For every chapter find min and max probability and fetch question according to our algorithm.
        String[] chapters = (String[])details.get("chapters");
        for(String chapter: (String[])details.get("chapters")) {   
            DatabaseHelper dh = (DatabaseHelper)di.get("databaseHelper");
            QuestionDetails qd = new QuestionDetails(dh.getChapterFromName(chapter));
            float maxProbability=0;
            try {
                Database db = (Database)di.get("database");
                ResultSet rs = null;
                
                // Fetching max probability from questions for that chapter
                String maxProbabilityQuery = "SELECT max(probability) as max_probability FROM questions WHERE chapter_id = " + qd.getChapterId();
                rs = db.query(maxProbabilityQuery);
                if(rs.next()) {
                    maxProbability = rs.getFloat("max_probability");
                }
                
                // adding into chapterDetails ArrayList
                if("EASY".equals(details.get("level"))) {
                    QuestionDetails tempDetails = fetchQuestionsForPaper("EASY", (String)details.get("type"), Constants.EASY_SEGREGATE, maxProbability, qd);
                    chapterDetails.add(tempDetails);
                } else if("MEDIUM".equals(details.get("level"))) {
                    chapterDetails.add(fetchQuestionsForPaper("MEDIUM", (String)details.get("type"), Constants.MEDIUM_SEGREGATE, maxProbability, qd));
                } else if("HARD".equals(details.get("level"))) {
                    chapterDetails.add(fetchQuestionsForPaper("HARD", (String)details.get("type"), Constants.HARD_SEGREGATE, maxProbability, qd));
                }
                
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        }
        return chapterDetails;
    }
    
    private static QuestionDetails fetchQuestionsForPaper(String level, String type, int[] SEGREGATE, float maxProbability, QuestionDetails qd) {
        String marks1 = "", marks2 = "";
        if("MCQ".equalsIgnoreCase(type)) {
            marks1 = "1";
            marks2 = "2";
        } else if("NORMAL".equalsIgnoreCase(type)) {
            marks1 = "5";
            marks2 = "10";
        }
        float tempMax = maxProbability;
        String[][] levelsArray = null;
        if(level.equalsIgnoreCase("Easy")) {
            levelsArray = Constants.EASY_PAPER;
        } else if(level.equalsIgnoreCase("Medium")) {
            levelsArray = Constants.MEDIUM_PAPER;
        } else if(level.equalsIgnoreCase("Hard")) {
            levelsArray = Constants.HARD_PAPER;
        }
        for(int i=0; i<SEGREGATE.length; ++i) {
            try {
                // finding the percentage after seggregation.
                float seggregatePercentage = (maxProbability/100)*SEGREGATE[i];
                float minProbability = (float) (tempMax - (Math.round(seggregatePercentage*100)/100.0d));
                Database db = (Database)di.get("database");
                ArrayList<String> levelsList = new ArrayList<>();
                Collections.addAll(levelsList, levelsArray[i]);
                String levels = db.implodeFromListWithQuotes(levelsList);
                // Fetching set of questions of that chapter with limits of probability.
                ResultSet rsFirst = db.table("questions").where("AND", "questions.difficulty_level IN ~(" + levels + ")", "questions.probability <= " + tempMax, "questions.probability >= " + minProbability, "questions.chapter_id = " + qd.getChapterId(), "questions.marks = " + marks1).get();
                // Fetching set of 10 marks questions of that chapter with limits of probability.
                ResultSet rsSecond = db.table("questions").where("AND", "questions.difficulty_level IN ~(" + levels + ")", "questions.probability <= " + tempMax, "questions.probability >= " + minProbability, "questions.chapter_id = " + qd.getChapterId(), "questions.marks = " + marks2).get();
                int marks = qd.getMarks();
                Long seggregateMarks = Math.round(Math.ceil((marks/100.0f)*SEGREGATE[i]));
                marks = (int) ((Math.round(seggregateMarks.intValue()*100)/100.0d));
                
                rsFirst.last();
                int rows = rsFirst.getRow();
                
                //if there are no fetched rows as per constraints then fetch all questions and update the ResultSet
                if(rows == 0 || rows == 1 || rows <= qd.sizeOfSet1Questions()) {
                    rsFirst = fetchRandomQuestionIrrespectiveOfProbability(qd, Integer.parseInt(marks1), level);
                    rsFirst.last();
                    rows = rsFirst.getRow();
                }
                
                // Set 1 insertion
                qd.setPendingMarks(marks);
                while(qd.getPendingMarks() > 0) {
                    int randomRow = ThreadLocalRandom.current().nextInt(0, rows+1);
                    rsFirst.absolute(randomRow);
                    if(! marks1.equals(rsFirst.getString("questions.marks"))) {
                        continue;
                    }
                    if(! qd.checkIfQuestionExistsInSet1(rsFirst.getInt("questions.id"))) {
                        ArrayList<OptionPOJO> options = new ArrayList<>();
                        if(rsFirst.getString("questions.type").equalsIgnoreCase(Constants.MCQ)) {
                            ResultSet optionSet = db.table("question_options").where("AND", "question_id = " + rsFirst.getInt("questions.id")).get();
                            while(optionSet.next()) {
                                options.add(new OptionPOJO(optionSet.getInt("question_options.id"), optionSet.getString("question_options.title"), 0));
                            }
                        }
                        qd.pushQuestionInSet1(new QuestionPOJO(rsFirst.getInt("questions.id"), rsFirst.getString("questions.title"), rsFirst.getString("questions.difficulty_level"), rsFirst.getInt("questions.marks"), rsFirst.getString("questions.type"), rsFirst.getInt("questions.probability"), qd.getChapter(), options));
                    }
                }
                
                // Set 2 insertion
                // setting pending marks again to default chapter weightage seggregated marks.
                qd.setPendingMarks(marks);
                rsSecond.last();
                rows = rsSecond.getRow();
                
                //if there are no fetched rows as per constraints then fetch all questions and update the ResultSet
                if(rows == 0 || rows == 1 || rows <= qd.sizeOfSet2Questions()) {
                    rsSecond = fetchRandomQuestionIrrespectiveOfProbability(qd, Integer.parseInt(marks2), level);
                    rsSecond.last();
                    rows = rsSecond.getRow();
                }
                while(qd.getPendingMarks() > 0) {
                    int randomRow = ThreadLocalRandom.current().nextInt(0, rows+1);
                    rsSecond.absolute(randomRow);
                    if(! marks2.equals(rsSecond.getString("questions.marks"))) {
                        continue;
                    }
                    if(! qd.checkIfQuestionExistsInSet2(rsSecond.getInt("questions.id"))) {
                        ArrayList<OptionPOJO> options = new ArrayList<>();
                        if(rsFirst.getString("questions.type").equalsIgnoreCase(Constants.MCQ)) {
                            ResultSet optionSet = db.table("question_options").where("AND", "question_id = " + rsSecond.getInt("questions.id")).get();
                            while(optionSet.next()) {
                                options.add(new OptionPOJO(optionSet.getInt("question_options.id"), optionSet.getString("question_options.title"), 0));
                            }
                        }
                        qd.pushQuestionInSet2(new QuestionPOJO(rsSecond.getInt("questions.id"), rsSecond.getString("questions.title"), rsSecond.getString("questions.difficulty_level"), rsSecond.getInt("questions.marks"), rsSecond.getString("questions.type"), rsSecond.getInt("questions.probability"), qd.getChapter(), options));
                    }
                }
                tempMax = minProbability;
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        }
        return qd;
    }
  
    private static ResultSet fetchRandomQuestionIrrespectiveOfProbability(QuestionDetails qd, int marks, String level) {
        Database db = (Database)di.get("database");
        ResultSet rs = db.table("questions,chapters").where("AND", "questions.difficulty_level = " + level,  "questions.chapter_id = " + qd.getChapterId(), "questions.marks = " + marks).get();
        return rs;
    }
    
    public static HashMap<String, ArrayList<QuestionPOJO>> allocateQuestions(ArrayList<QuestionDetails> chapterDetails, int marks, String type) {   
        // Sorting chapterDetails according to weight
        Collections.sort(chapterDetails);
        ArrayList<QuestionPOJO> set1Questions = null, set2Questions = null;
        int set1QuestionsCapacity = 0, set2QuestionsCapacity = 0;
        int marks1 = 0, marks2 = 0;
        String m1 = "", m2 = "";
        if(marks == Constants.SEM_MARKS) {
            if(type.equalsIgnoreCase(Constants.NORMAL)) {
                m1 = "5";
                m2 = "10";
                marks1 = Constants.FIVE_MARKS_QUESTIONS_IN_80_MARKS;
                marks2 = Constants.TEN_MARKS_QUESTIONS_IN_80_MARKS;
            } else if(type.equalsIgnoreCase(Constants.MCQ)) {
                m1 = "1";
                m2 = "2";
                marks1 = Constants.ONE_MARKS_QUESTIONS_IN_80_MARKS_MCQ;
                marks2 = Constants.TWO_MARKS_QUESTIONS_IN_80_MARKS_MCQ;
            }
            set1Questions = new ArrayList<>(marks1);
            set2Questions = new ArrayList<>(marks2);
            set1QuestionsCapacity = marks1;
            set2QuestionsCapacity = marks2;
        } else if(marks == Constants.UT_MARKS) {
            if(type.equalsIgnoreCase(Constants.NORMAL)) {
                m1 = "5";
                m2 = "10";
                marks1 = Constants.FIVE_MARKS_QUESTIONS_IN_20_MARKS;
                marks2 = Constants.TEN_MARKS_QUESTIONS_IN_20_MARKS;
            } else if(type.equalsIgnoreCase(Constants.MCQ)) {
                m1 = "1";
                m2 = "2";
                marks1 = Constants.ONE_MARKS_QUESTIONS_IN_20_MARKS_MCQ;
                marks2 = Constants.TWO_MARKS_QUESTIONS_IN_20_MARKS_MCQ;
            }
            set1Questions = new ArrayList<>(marks1);
            set2Questions = new ArrayList<>(marks2);
            set1QuestionsCapacity = marks1;
            set2QuestionsCapacity = marks2;
        }
        //  *** 5 marks allocating ***
        
        // Step 1: Add atleast one question per chapter in ArrayList.
        // Checking if chapter count exceeds that Five Mark question count!
        if(chapterDetails.size() <= set1QuestionsCapacity) {
            for(QuestionDetails qd: chapterDetails) {
               set1Questions.add(qd.randomQuestionFromQuestionSet1());
            }
        }
        
        //Step 2: fetching remaining questions from chapters with high weigtage with 75% questions and lowest weightage with 25% questions
        int remainingQuestionsCount = set1QuestionsCapacity - set1Questions.size();
        int maximumQuestionsCount =  (int) Math.round(Math.ceil((remainingQuestionsCount/100.0f)*Constants.MAXIMUM_REMAINING_QUESTION_PERCENTAGE));
        int minimumQuestionsCount = (int) Math.round(Math.floor((remainingQuestionsCount/100.0f)*Constants.MINIMUM_REMAINING_QUESTION_PERCENTAGE));
        int maximumChapterCount =  (int) Math.round(Math.ceil(((chapterDetails.size()-1)/100.0f)*Constants.MAXIMUM_REMAINING_QUESTION_PERCENTAGE));
        int minimumChapterCount = (int) Math.round(Math.floor(((chapterDetails.size()-1)/100.0f)*Constants.MINIMUM_REMAINING_QUESTION_PERCENTAGE));
        if(remainingQuestionsCount != 0) {
            while(set1Questions.size() != set1QuestionsCapacity) {
                int tempCount = maximumQuestionsCount;
                
                // For 75% of remaining questions fetch from 75% chapters having highest weight
                while(tempCount != 0) {
                    int randomChapter = ThreadLocalRandom.current().nextInt(0, maximumChapterCount);
                    set1Questions.add(chapterDetails.get(randomChapter).randomQuestionFromQuestionSet1());
                    tempCount--;
                    remainingQuestionsCount--;
                }
                tempCount = minimumQuestionsCount;
                
                // For 25% of remaining questions fetch from 25% remaining chapters
                while(tempCount != 0) {
                    QuestionPOJO question = null;
                    if(! (maximumChapterCount == chapterDetails.size()-1)) {
                        int randomChapter = ThreadLocalRandom.current().nextInt(maximumChapterCount, chapterDetails.size()-1);
                        question = chapterDetails.get(randomChapter).randomQuestionFromQuestionSet1();
                    } else {
                        question = chapterDetails.get(chapterDetails.size()-1).randomQuestionFromQuestionSet1();
                    }
                    if(question != null)
                        set1Questions.add(question);
                    tempCount--;
                    remainingQuestionsCount--;
                }
            }
        }
        
        
        //  *** 10 marks allocating ***
        
        // Step 1: Add atleast one question per chapter in ArrayList.
        // Checking if chapter count exceeds that Five Mark question count!
        if(chapterDetails.size() <= set2QuestionsCapacity) {
            for(QuestionDetails qd: chapterDetails) {
               set2Questions.add(qd.randomQuestionFromQuestionSet2());
            }
        }
        
        //Step 2: fetching remaining questions from chapters with high weigtage with 75% questions and lowest weightage with 25% questions
        remainingQuestionsCount = set2QuestionsCapacity - set2Questions.size();
        maximumQuestionsCount =  (int) Math.round(Math.ceil((remainingQuestionsCount/100.0f)*Constants.MAXIMUM_REMAINING_QUESTION_PERCENTAGE));
        minimumQuestionsCount = (int) Math.round(Math.floor((remainingQuestionsCount/100.0f)*Constants.MINIMUM_REMAINING_QUESTION_PERCENTAGE));
        if(remainingQuestionsCount != 0) {
            while(set2Questions.size() != set2QuestionsCapacity) {
                int tempCount = maximumQuestionsCount;
                
                // For 75% of remaining questions fetch from 75% chapters having highest weight
                while(tempCount != 0) {
                    int randomChapter = ThreadLocalRandom.current().nextInt(0, maximumChapterCount);
                    set2Questions.add(chapterDetails.get(randomChapter).randomQuestionFromQuestionSet2());
                    tempCount--;
                    remainingQuestionsCount--;
                }
                tempCount = minimumQuestionsCount;
                
                // For 25% of remaining questions fetch from 25% remaining chapters
                while(tempCount != 0) {
                    QuestionPOJO question = null;
                    if(! (maximumChapterCount == chapterDetails.size()-1)) {
                        int randomChapter = ThreadLocalRandom.current().nextInt(maximumChapterCount, chapterDetails.size()-1);
                        question = chapterDetails.get(randomChapter).randomQuestionFromQuestionSet2();
                    } else {
                        question = chapterDetails.get(chapterDetails.size()-1).randomQuestionFromQuestionSet2();
                    }
                    if(question != null)
                        set2Questions.add(question);
                    tempCount--;
                    remainingQuestionsCount--;
                }
            }
        }
        PDFHelper.generatePdf(type, marks, chapterDetails.get(0).getChapter().getSubject().getName(), set1Questions, set2Questions);
        HashMap<String, ArrayList<QuestionPOJO>> questionSets = new HashMap<>();
        questionSets.put(m1 + "markQuestions", set1Questions);
        questionSets.put(m2 + "markQuestions", set2Questions);
        return questionSets;
    }
    
    public static void generatePdf(String type, int marks, String subject, ArrayList<QuestionPOJO> set1Questions, ArrayList<QuestionPOJO> set2Questions)
    {
        try {
            OutputStream file = null;
            String name = "question_paper_"+(new Date().getTime())+".pdf";
            file = new FileOutputStream(new File(name));
            Document document = new Document();
            PdfWriter.getInstance(document, file);
            document.open();
            Paragraph collegeName = new Paragraph("Thadomal Shahani Engineering College");
            Paragraph subjectName = new Paragraph("Subject : " + subject);
            Paragraph marksLabel = new Paragraph("Marks : " + marks);
            subjectName.setAlignment(Element.ALIGN_CENTER);
            collegeName.setAlignment(Element.ALIGN_CENTER);
            marksLabel.setAlignment(Element.ALIGN_RIGHT);
            document.add(collegeName);
            document.add(subjectName);
            document.add(marksLabel);
            document.addTitle("Subject: " + subject);
            LineSeparator ls = new LineSeparator();
            ls.setOffset(-10);
            document.add(ls);
            
            if(type.equalsIgnoreCase(Constants.NORMAL)) {
                if(marks == Constants.SEM_MARKS) {
                    Paragraph attempt1 = new Paragraph("\nQ1. Attempt any 2 out of 4 \t\t (5 marks)\n\t");
                    Paragraph attempt2 = new Paragraph("\nQ2. Attempt any 3 out of 4 \t\t (5 marks)\n\t");
                    Paragraph attempt3 = new Paragraph("\nQ3. Attempt any 3 out of 4 \t\t (5 marks)\n\t");
                    Paragraph attempt4 = new Paragraph("\nQ4. Attempt any 2 out of 3 \t\t (10 marks)\n\t");
                    Paragraph attempt5 = new Paragraph("\nQ5. Attempt any 2 out of 3 \t\t (10 marks)\n\t");
                    document.add(attempt1);
                    for(int i=0; i<4; ++i) {
                        PDFHelper.insertQuestionInPdf(document, i+1, set1Questions.get(i), i==0);
                    }
                    document.add(attempt2);
                    for(int i=4, j=0; i<8; ++i, ++j) {
                        PDFHelper.insertQuestionInPdf(document, j+1, set1Questions.get(i), j==0);
                    }
                    document.add(attempt3);
                    for(int i=8, j=0; i<12; ++i, ++j) {
                        PDFHelper.insertQuestionInPdf(document, j+1, set1Questions.get(i), j==0);
                    }
                    document.add(attempt4);
                    for(int i=0; i<3; ++i) {
                        PDFHelper.insertQuestionInPdf(document, i+1, set2Questions.get(i), i==0);
                    }
                    document.add(attempt5);
                    for(int i=3, j=0; i<6; ++i, ++j) {
                        PDFHelper.insertQuestionInPdf(document, j+1, set2Questions.get(i), j==0);
                    }
                } else if(marks == Constants.UT_MARKS) {
                    Paragraph attempt1 = new Paragraph("\nQ1. Attempt any 2 out of 3 \t\t (5 marks)");
                    Paragraph attempt2 = new Paragraph("\nQ2. Attempt any 1 out of 2 \t\t (10 marks)");

                    document.add(attempt1);
                    for(int i=0; i<3; ++i) {
                        PDFHelper.insertQuestionInPdf(document, i+1, set1Questions.get(i), i==0);
                    }
                    document.add(attempt2);
                    for(int i=0; i<2; ++i) {
                        PDFHelper.insertQuestionInPdf(document, i+1, set2Questions.get(i), i==0);
                    }
                }
            } else if(type.equalsIgnoreCase(Constants.MCQ)) {
                if(marks == Constants.SEM_MARKS) {
                    Paragraph attempt1 = new Paragraph("\nQ1. Solve Multiple Choice Questions:- \t\t (1 marks)");
                    Paragraph attempt2 = new Paragraph("\nQ2. Solve Multiple Choice Questions:- \t\t (2 marks)");

                    document.add(attempt1);
                    for(int i=0; i<Constants.ONE_MARKS_QUESTIONS_IN_80_MARKS_MCQ; ++i) {
                        PDFHelper.insertQuestionInPdf(document, i+1, set1Questions.get(i), true);
                    }
                    document.add(attempt2);
                    for(int i=0; i<Constants.TWO_MARKS_QUESTIONS_IN_80_MARKS_MCQ; ++i) {
                        PDFHelper.insertQuestionInPdf(document, i+1, set2Questions.get(i), true);
                    }
                } else if(marks == Constants.UT_MARKS) {
                    Paragraph attempt1 = new Paragraph("\nQ1. Solve Multiple Choice Questions:- \t\t (1 marks)");
                    Paragraph attempt2 = new Paragraph("\nQ2. Solve Multiple Choice Questions:- \t\t (2 marks)");

                    document.add(attempt1);
                    for(int i=0; i<Constants.ONE_MARKS_QUESTIONS_IN_20_MARKS_MCQ; ++i) {
                        PDFHelper.insertQuestionInPdf(document, i+1, set1Questions.get(i), true);
                    }
                    document.add(attempt2);
                    for(int i=0; i<Constants.TWO_MARKS_QUESTIONS_IN_20_MARKS_MCQ-1; ++i) {
                        PDFHelper.insertQuestionInPdf(document, i+1, set2Questions.get(i), true);
                    }
                }
            }
            document.addCreationDate();
            document.close();
            JOptionPane.showMessageDialog(null, "Question Paper has been Generated Successfully!");
        } catch (FileNotFoundException | DocumentException ex) {
            JOptionPane.showMessageDialog(null, "There was some problem while generating Question Paper!");
        }
    }
    
    private static void insertQuestionInPdf(Document document, int questionNumber, QuestionPOJO question, boolean isFirst) {
        try {
            Paragraph q = new Paragraph(((isFirst) ? "\n\t" : "") + questionNumber + ". " + question.getTitle());
            document.add(q);
            if(question.getType().equals(Constants.MCQ)) {
                char ch = 'a';
                for(OptionPOJO option: question.getOptions()) {
                    Paragraph temp = new Paragraph(((ch == 'a') ? "\n\t" : "") + ch++ + ") " + option.getTitle());
                    document.add(temp);
                }
            }
        } catch (DocumentException ex) {
            System.out.println(ex);
        }
    }
}

