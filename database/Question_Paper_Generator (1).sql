CREATE TABLE `users` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `username` varchar(255),
  `email` varchar(255),
  `password` varchar(255)
);

CREATE TABLE `branches` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `subjects` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `chapters` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `subject_id` int
);

CREATE TABLE `semesters` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `questions` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `title` varchar(255),
  `difficulty_level` ENUM ('EASY', 'MEDIUM', 'HARD'),
  `marks` int,
  `chapter_id` int,
  `type` tinyint,
  `is_active` tinyint,
  `probability` int
);

CREATE TABLE `question_options` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `title` varchar(255),
  `question_id` int,
  `is_correct` tinyint
);

CREATE TABLE `question_paper` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `content` json
);

ALTER TABLE `questions` ADD FOREIGN KEY (`chapter_id`) REFERENCES `chapters` (`id`);

ALTER TABLE `question_options` ADD FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`);
